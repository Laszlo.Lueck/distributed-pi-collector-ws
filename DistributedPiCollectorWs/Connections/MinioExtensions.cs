using DistributedPiCollectorWs.Configuration;
using DistributedPiCollectorWs.Services;
using SimpleMinioClient;

namespace DistributedPiCollectorWs.Connections;

public static class MinioExtensions
{
    public static void AddMinioFromEnvironment(this IServiceCollection services,
        IConfigurationHandler configurationHandler)
    {
        var connections = StaticHelper.LoadMinioConfigurationFromEnvironment(configurationHandler);
        var connectionClients = connections
            .Select(tpl =>
            {
                var s = new SimpleMinioConfiguration(tpl.Value.Host, tpl.Value.AccessKey, tpl.Value.SecretKey);
                var client = new SimpleMinioClient.SimpleMinioClient(s);
                return (tpl.Key, client);
            })
            .ToDictionary(tpl => tpl.Key, tpl => tpl.client);


        services.AddKeyedSingleton<IStorageCacheAsync>(
            MinioBucketName.PartBucket.Name,
            (prov, _) => new BaseStorageCacheMinio(
                prov.GetRequiredService<ILogger<BaseStorageCacheMinio>>(),
                connectionClients[MinioBucketName.PartBucket.ConnectionName],
                MinioBucketName.PartBucket.Path));

        services.AddKeyedSingleton<IStorageCacheAsync>(
            MinioBucketName.ResultBucket.Name,
            (prov, _) =>
                new BaseStorageCacheMinio(prov.GetRequiredService<ILogger<BaseStorageCacheMinio>>(),
                    connectionClients[MinioBucketName.ResultBucket.ConnectionName],
                    MinioBucketName.ResultBucket.Path));

        services.AddKeyedSingleton<IStorageCacheAsync>(
            MinioBucketName.FactorialBucket.Name,
            (prov, _) =>
                new BaseStorageCacheMinio(prov.GetRequiredService<ILogger<BaseStorageCacheMinio>>(),
                    connectionClients[MinioBucketName.FactorialBucket.ConnectionName],
                    MinioBucketName.FactorialBucket.Path));
    }
}