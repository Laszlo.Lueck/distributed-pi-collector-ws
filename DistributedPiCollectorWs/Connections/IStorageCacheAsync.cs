using LanguageExt;

namespace DistributedPiCollectorWs.Connections;

public interface IStorageCacheAsync
{
    string GetConnection();
    
    Task<bool> IsKeyExistAsync(string key);

    Task<Option<byte[]>> GetItemFromCacheAsync(string id);


    Task SetItemToCacheAsync(string id, byte[] value);
}