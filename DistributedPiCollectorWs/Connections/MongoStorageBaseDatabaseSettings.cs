﻿namespace DistributedPiCollectorWs.Connections;

public class MongoStorageBaseDatabaseSettings
{
    public string? DatabaseName;
    public string? ConnectionString;
    public string? CollectionName;
    public string? UserName;
    public string? Password;
}

