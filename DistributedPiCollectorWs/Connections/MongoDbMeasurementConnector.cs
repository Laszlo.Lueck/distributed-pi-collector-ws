using DistributedPiCollectorWs.Services;
using LanguageExt;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace DistributedPiCollectorWs.Connections;

public class MongoDbMeasurementConnector<T> : IDatabaseConnector<T>
{
    private readonly ILogger<MongoDbMeasurementConnector<T>> _logger;

    public IMongoCollection<T> MongoCollection { get; }

    public MongoDbMeasurementConnector(ILogger<MongoDbMeasurementConnector<T>> logger, IOptions<MongoStorageMeasurementsDatabaseSettings> databaseSettings)
    {   
        _logger = logger;
        var cs = StaticHelper
            .CreateMongoConnection(databaseSettings.Value.ConnectionString,
            databaseSettings.Value.UserName, databaseSettings.Value.Password);
        var mongoClient = new MongoClient(cs);
        var mongoDatabase = mongoClient.GetDatabase(databaseSettings.Value.DatabaseName);
        MongoCollection = mongoDatabase.GetCollection<T>(databaseSettings.Value.CollectionName);
    }

    public async Task<IEnumerable<T>> FindManyAsync(FilterDefinition<T> filterDefinition)
    {
        _logger.LogInformation("find many entities");
        var asyncCursor = await MongoCollection.FindAsync(filterDefinition);
        return asyncCursor.ToEnumerable();
    }

    public Task InsertManyAsync(IEnumerable<T> values)
    {
        _logger.LogInformation("insert many entities");
        return MongoCollection.InsertManyAsync(values);
    }

    public async Task<Option<T>> FindOneAsync(FilterDefinition<T> filterDefinition)
    {
        _logger.LogInformation("find one entity");
        var res = await MongoCollection.Find(filterDefinition).FirstOrDefaultAsync();
        return res;
    }

    public Task InsertOneAsync(T value)
    {
        _logger.LogInformation("insert one entity");
        return MongoCollection.InsertOneAsync(value);
    }

    public Task<UpdateResult> UpdateOneAsync(FilterDefinition<T> filterDefinition, UpdateDefinition<T> newValue)
    {
        _logger.LogInformation("update one entity");
        return MongoCollection.UpdateOneAsync(filterDefinition, newValue);
    }

    public Task<DeleteResult> DeleteOneAsync(FilterDefinition<T> filterDefinition)
    {
        _logger.LogInformation("delete one entity");
        return MongoCollection.DeleteOneAsync(filterDefinition);
    }

    public Task<DeleteResult> DeleteManyAsync(FilterDefinition<T> filterDefinition)
    {
        _logger.LogInformation("delete many entities");
        return MongoCollection.DeleteManyAsync(filterDefinition);
    }
}