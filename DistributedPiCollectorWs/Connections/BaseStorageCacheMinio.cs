using LanguageExt;
using SimpleMinioClient;
using static LanguageExt.Prelude;

namespace DistributedPiCollectorWs.Connections;

public class BaseStorageCacheMinio(
    ILogger logger,
    ISimpleMinioClient minioClient,
    string? bucketName) : IStorageCacheAsync
{
    public string GetConnection() => minioClient.MinioUrl;

    public async Task<bool> IsKeyExistAsync(string key)
    {
        try
        {
            if (bucketName is null)
                throw new InvalidOperationException("bucket name is not set");
            
            var keyExistingResult = await minioClient.IsObjectAvailableAsync(bucketName, key);
            switch (keyExistingResult.IsSuccess)
            {
                case true:
                    return true;
                case false when keyExistingResult.HttpStatusCode == 404:
                    return false;
                default:
                    logger.LogWarning("error while check an object in objectstore: {Error} :: {StatusCode}",
                        keyExistingResult.ErrorMessage, keyExistingResult.HttpStatusCode);
                    return false;
            }
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "error while check an object in objectstore");
            throw;
        }
    }

    public async Task<Option<byte[]>> GetItemFromCacheAsync(string id)
    {
        try
        {
            if (bucketName is null)
                throw new InvalidOperationException("bucket name is not set");
            
            var operationResult = await minioClient.GetObjectAsync(bucketName, id);
            if (operationResult.IsSuccess)
            {
                var ms = new MemoryStream();
                return await Optional(operationResult.Data)
                    .Match(
                        async data =>
                        {
                            await data.CopyToAsync(ms);
                            return Option<byte[]>.Some(ms.ToArray());
                        }, () =>
                        {
                            logger.LogWarning("unexpected data received result stream is null");
                            return Task.FromResult(Option<byte[]>.None);
                        });
            }

            logger.LogWarning(
                "unexpected result from minio client received status code {OperationResultHttpStatusCode}, message: {OperationResultErrorMessage}",
                operationResult.HttpStatusCode, operationResult.ErrorMessage);
            return None;
        }
        catch (Exception exception)
        {
            logger.LogError(exception, "error while read an object in objectstore");
            return None;
        }
    }

    public async Task SetItemToCacheAsync(string id, byte[] value)
    {
        try
        {
            if (bucketName is null)
                throw new InvalidOperationException("bucket name is not set");


            var ms = new MemoryStream(value ?? throw new InvalidOperationException());
            var updateResult = await minioClient.PutObjectAsync(bucketName, id, ms);
            if (!updateResult.IsSuccess)
                logger.LogWarning("error while write an object in objectstore: {Error} :: {StatusCode}",
                    updateResult.ErrorMessage, updateResult.HttpStatusCode);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "error while write an object in objectstore");
            throw;
        }
    }
}