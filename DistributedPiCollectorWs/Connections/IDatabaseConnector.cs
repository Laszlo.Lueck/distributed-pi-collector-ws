using LanguageExt;
using MongoDB.Driver;

namespace DistributedPiCollectorWs.Connections;

public interface IDatabaseConnector<T>
{
    IMongoCollection<T> MongoCollection { get; }
    Task<IEnumerable<T>> FindManyAsync(FilterDefinition<T> filterDefinition);

    Task<Option<T>> FindOneAsync(FilterDefinition<T> filterDefinition);

    Task InsertOneAsync(T value);
    
    Task InsertManyAsync(IEnumerable<T> values);

    Task<UpdateResult> UpdateOneAsync(FilterDefinition<T> filterDefinition, UpdateDefinition<T> newValue);

    Task<DeleteResult> DeleteOneAsync(FilterDefinition<T> filterDefinition);

    Task<DeleteResult> DeleteManyAsync(FilterDefinition<T> filterDefinition);
}