using Confluent.Kafka;
using DistributedPiCollectorWs.Configuration;
using DistributedPiCollectorWs.Services.Incoming;
using DistributedPiCollectorWs.Services.Outgoing;
using Silverback.Messaging.Configuration;

namespace DistributedPiCollectorWs.Connections;

public class CalculationMiddlewareEndpointConfigurator(IConfigurationHandler configurationHandler)
    : IEndpointsConfigurator
{
    public void Configure(IEndpointsConfigurationBuilder builder)
    {
        builder
            .AddKafkaEndpoints(endpoints =>
                endpoints.Configure(config =>
                        config.BootstrapServers = configurationHandler.GetValue<string>("KAFKA_URL"))
                    .AddInbound(endpoint =>
                        endpoint
                            .ConsumeFrom(configurationHandler.GetValue<string>("KAFKA_TOPIC_JOB_RESPONSES")).Configure(
                                config =>
                                {
                                    config.GroupId = configurationHandler.GetValue<string>("KAFKA_GROUP_ID");
                                    config.AutoOffsetReset = AutoOffsetReset.Latest;
                                })
                            .DeserializeJson(ser => ser.UseFixedType<CalculatorResponseDto>()))
                    .AddOutbound<CalculatorRequestDto>(endpoint =>
                        endpoint.ProduceTo(configurationHandler.GetValue<string>("KAFKA_TOPIC_JOB_REQUESTS"))
                            .SerializeAsJson(ser => ser.UseFixedType<CalculatorRequestDto>()))
            );
    }
}