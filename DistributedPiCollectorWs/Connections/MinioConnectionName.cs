﻿namespace DistributedPiCollectorWs.Connections;

public enum MinioConnectionName
{
    MainConnection = 0,
    FactorialConnection = 1
}