using DistributedPiCollectorWs.Calculation;
using DistributedPiCollectorWs.Connections;
using DistributedPiCollectorWs.Services.States;
using DistributedPiCollectorWs.Services.Storage;
using DistributedPiCollectorWs.Services.Tos;

namespace DistributedPiCollectorWs.Services;

public class CreateMeasurement(
    ILogger<CreateMeasurement> logger,
    IDatabaseConnector<MeasurementDocument> dbConnector,
    ICalculatorTest calculatorTest) : ICreateMeasurement
{
    public async Task<CreateMeasurementServiceResultTo> CreateMeasurementAsync(
        CreateMeasurementServiceTo createMeasurementServiceTo)
    {
        var session = Guid.NewGuid();
        logger.LogInformation("first check if processing can start with those parameters");
        var canProcess = await calculatorTest.CheckPrecisionLengthAsync(createMeasurementServiceTo.Iterations,
            createMeasurementServiceTo.Precision);
        if (!canProcess)
        {
            logger.LogInformation("precision too low, gave up");
            return new CreateMeasurementServiceResultTo(State: CreateMeasurementState.PrecisionTooLowError,
                AdditionalText:
                $"precision {createMeasurementServiceTo.Precision} :: iterations {createMeasurementServiceTo.Iterations}",
                session);
        }


        logger.LogInformation("create main entry for the measurement");
        var doc = new MeasurementDocument(session, DateTime.Now, createMeasurementServiceTo.Iterations,
            createMeasurementServiceTo.Precision, false, 0, true, ExecState.New);
        await dbConnector.InsertOneAsync(doc);

        return new CreateMeasurementServiceResultTo(CreateMeasurementState.Ok,
            $"create calculation for session {session}", session);
    }


}