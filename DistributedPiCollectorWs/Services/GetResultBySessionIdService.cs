using System.Numerics;
using DistributedPiCollectorWs.Connections;
using LanguageExt;

namespace DistributedPiCollectorWs.Services;

public class GetResultBySessionIdService(IServiceProvider serviceProvider)
    : IGetResultBySessionId
{
    private readonly IStorageCacheAsync _mainRegistry =
        serviceProvider.GetRequiredKeyedService<IStorageCacheAsync>(MinioBucketName.ResultBucket.Name);

    public async Task<Option<string>> GetResultBySessionIdAsync(Guid sessionId)
    {
        var resultOpt = await _mainRegistry
            .GetItemFromCacheAsync(sessionId.ToString());
        return resultOpt
            .Map(x => new BigInteger(x ?? throw new ArgumentNullException(nameof(x))).ToString());
    }
}