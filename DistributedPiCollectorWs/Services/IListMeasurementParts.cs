using DistributedPiCollectorWs.Endpoints.ListMeasurementParts;
using DistributedPiCollectorWs.Services.Tos;

namespace DistributedPiCollectorWs.Services;

public interface IListMeasurementParts
{
    Task<ListMeasurementPartsTo> GetListMeasurementPartsAsync(GetListMeasurementPartsRequest request, CancellationToken cancellationToken);
} 