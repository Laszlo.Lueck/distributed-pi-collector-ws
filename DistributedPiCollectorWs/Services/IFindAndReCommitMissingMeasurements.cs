﻿namespace DistributedPiCollectorWs.Services;

public interface IFindAndReCommitMissingMeasurements
{
    Task FindAndReCommitMissingMeasurementsAsync(Guid sessionId);
}