using DistributedPiCollectorWs.Services.Tos;

namespace DistributedPiCollectorWs.Services;

public interface IDeleteMeasurement
{
    Task<DeleteMeasurementResultTo> DeleteMeasurementAsync(Guid sessionId);
}