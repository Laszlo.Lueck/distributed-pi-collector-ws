using DistributedPiCollectorWs.Connections;
using DistributedPiCollectorWs.Services.Storage;
using DistributedPiCollectorWs.Services.Tos;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DistributedPiCollectorWs.Services;

public class MeasurementDistributionService(
    ILogger<MeasurementDistributionService> logger,
    IDatabaseConnector<MeasurementPartDocument> dbPartConnector) : IMeasurementDistribution
{
    public async Task<ListMeasurementDistributionTo> GetMeasurementDistributionAsync(Guid sessionId)
    {
        logger.LogInformation("get measurement distribution for session {SessionId}", sessionId);

        var pipeline = new[]
        {
            new BsonDocument("$match", new BsonDocument("session", new BsonBinaryData(sessionId, GuidRepresentation.Standard))),
            new BsonDocument("$match", new BsonDocument("processTimeMs", new BsonDocument("$exists", true))),
            new BsonDocument("$match", new BsonDocument("processTimeMs", new BsonDocument("$ne", BsonNull.Value))),
            new BsonDocument("$match", new BsonDocument("processTimeMs", new BsonDocument("$gt", 0))),
            new BsonDocument("$group", new BsonDocument
            {
                { "_id", "$hostName" },
                {
                    "processTimes", new BsonDocument("$push", new BsonDocument
                    {
                        { "iteration", "$iteration" },
                        { "processTimeMs", "$processTimeMs" }
                    })
                },
                { "totalProcessTime", new BsonDocument("$sum", "$processTimeMs") }
            }),
            new BsonDocument("$sort", new BsonDocument("processTimes.iteration", 1))
        };

        var asyncCursor = await dbPartConnector
            .MongoCollection
            .AggregateAsync<BsonDocument>(pipeline);

        var converted = asyncCursor
            .ToEnumerable()
            .Select(doc =>
                {
                    var hostName = doc["_id"].AsString;
                    var pTimes = doc["processTimes"]
                        .AsBsonArray
                        .Select(pt => (pt["iteration"].AsInt32, pt["processTimeMs"].AsDouble))
                        .OrderBy(pt => pt.Item1)
                        .ToList();
                    var totalProcessTime = doc["totalProcessTime"].AsDouble;
                    return new MeasurementDistributionTo(
                        pTimes.Select(d => new MeasurementEntry(d.AsInt32, d.AsDouble)).ToList(), totalProcessTime,
                        hostName);
                }
            );

        return new ListMeasurementDistributionTo(sessionId, converted.ToList());
    }
}