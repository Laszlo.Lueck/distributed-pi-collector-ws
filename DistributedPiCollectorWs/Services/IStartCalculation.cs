﻿using DistributedPiCollectorWs.Services.Tos;

namespace DistributedPiCollectorWs.Services;

public interface IStartCalculation
{
    Task StartCalculationAsync(StartCalculationTo startCalculationTo, CancellationToken cancellationToken);
}