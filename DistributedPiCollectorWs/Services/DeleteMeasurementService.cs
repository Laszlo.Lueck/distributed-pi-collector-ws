using DistributedPiCollectorWs.Connections;
using DistributedPiCollectorWs.Services.Storage;
using DistributedPiCollectorWs.Services.Tos;
using MongoDB.Driver;

namespace DistributedPiCollectorWs.Services;

public class DeleteMeasurementService(
    ILogger<DeleteMeasurementService> logger,
    IDatabaseConnector<MeasurementDocument> dbConnector,
    IDatabaseConnector<MeasurementPartDocument> dbPartConnector) : IDeleteMeasurement
{
    public async Task<DeleteMeasurementResultTo> DeleteMeasurementAsync(Guid sessionId)
    {
        logger.LogInformation("delete session {SessionId} from measurement first", sessionId);

        var fd = Builders<MeasurementDocument>.Filter.Eq(d => d.Session, sessionId);
        var fdPart = Builders<MeasurementPartDocument>.Filter.Eq(d => d.Session, sessionId);

        var deleteMeasurementResult = await dbConnector.DeleteOneAsync(fd);
        var deleteMeasurementPartResult = await dbPartConnector.DeleteManyAsync(fdPart);

        return new DeleteMeasurementResultTo(SessionId: sessionId,
            MeasurementDeleteSuccess: deleteMeasurementResult is { IsAcknowledged: true, DeletedCount: > 0 },
            MeasurementPartDeleteSuccess: deleteMeasurementPartResult is { IsAcknowledged: true, DeletedCount: > 0 },
            MeasurementPartDeletedCount: deleteMeasurementPartResult.DeletedCount);
    }
}