using Confluent.Kafka;
using DistributedPiCollectorWs.Connections;
using DistributedPiCollectorWs.Services.Storage;
using MongoDB.Driver;

namespace DistributedPiCollectorWs.Services.Incoming;

public class CalculationResponseSubscriber(
    ILogger<CalculationResponseSubscriber> logger,
    IDatabaseConnector<MeasurementPartDocument> databasePartConnector,
    IDatabaseConnector<MeasurementDocument> dbConnector)
{
    public async Task OnMessageReceived(CalculatorResponseDto message)
    {
        try
        {
            logger.LogInformation("received id: {Id} for session {Session}", message.Id, message.Session);
            var filter = Builders<MeasurementPartDocument>
                .Filter
                .And(
                    Builders<MeasurementPartDocument>.Filter.Eq(s => s.Session, message.Session),
                    Builders<MeasurementPartDocument>.Filter.Eq(s => s.Iteration, message.Id)
                );
            var toUpdateOpt = await databasePartConnector.FindOneAsync(filter);

            await toUpdateOpt.Match(
                Some: async _ =>
                {
                    logger.LogInformation("found document for session {Session}", message.Session);
                    var cacheValue = StaticHelper.GenerateObjectKey(message.Id, message.Precision);
                    var update = Builders<MeasurementPartDocument>
                        .Update
                        .Set(d => d.ReturnValueHash, cacheValue)
                        .Set(d => d.ProcessTimeMs, message.ProcessTimeMs)
                        .Set(d => d.HostName, message.HostName)
                        .Set(d => d.ResultSize, message.LongLength);
                    await databasePartConnector.UpdateOneAsync(filter, update);

                    var aggFilter = Builders<MeasurementPartDocument>.Filter.And(
                        Builders<MeasurementPartDocument>.Filter.Eq(s => s.Session, message.Session),
                        Builders<MeasurementPartDocument>.Filter.Eq(s => s.ReturnValueHash, string.Empty)
                    );
                    var count = await databasePartConnector.MongoCollection.CountDocumentsAsync(aggFilter);
                    if (count == 0)
                    {
                        var udx = Builders<MeasurementDocument>.Update.Set(f => f.ExecState, ExecState.Finished);
                        var sessionFilter = Builders<MeasurementDocument>.Filter.Eq(s => s.Session, message.Session);
                        await dbConnector.UpdateOneAsync(sessionFilter, udx);
                    }
                },
                None: async () =>
                {
                    await Task.Run(() =>
                    {
                        logger.LogWarning("no document found for session {Session}", message.Session);
                    });
                });
        }
        catch (MessageNullException messageNullException)
        {
            logger.LogWarning(messageNullException, "Message is null");
        }
        catch (Exception e)
        {
            logger.LogError(e, "an error occured");
        }
    }
}