using System.Numerics;
using System.Text.Json.Serialization;
using MessagePack;

namespace DistributedPiCollectorWs.Services.Incoming;

public record CalculatorResponseDto(
    [property: JsonPropertyName("id")] int Id,
    [property: JsonPropertyName("session")] Guid Session,
    [property: JsonPropertyName("processTimeMs")] double ProcessTimeMs,
    [property: JsonPropertyName("hostName")] string HostName,
    [property: JsonPropertyName("precision")] int Precision,
    [property: JsonPropertyName("longLength")] long LongLength
);