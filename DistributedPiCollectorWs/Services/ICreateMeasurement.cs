using DistributedPiCollectorWs.Services.Tos;

namespace DistributedPiCollectorWs.Services;

public interface ICreateMeasurement
{
    Task<CreateMeasurementServiceResultTo>
        CreateMeasurementAsync(CreateMeasurementServiceTo createMeasurementServiceTo);
}