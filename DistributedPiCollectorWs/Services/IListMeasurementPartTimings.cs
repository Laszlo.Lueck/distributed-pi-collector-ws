using DistributedPiCollectorWs.Endpoints.ListMeasurementPartTimings;
using DistributedPiCollectorWs.Services.Tos;

namespace DistributedPiCollectorWs.Services;

public interface IListMeasurementPartTimings
{
    Task<IEnumerable<ListMeasurementPartTimingTo>> GetListMeasurementPartTimingsAsync(
        GetListMeasurementPartTimingsRequest request);
}