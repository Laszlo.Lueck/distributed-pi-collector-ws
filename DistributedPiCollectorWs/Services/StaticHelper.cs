using DistributedPiCollectorWs.Configuration;
using DistributedPiCollectorWs.Connections;
using DistributedPiCollectorWs.Services.Tos;
using LanguageExt;
using MongoDB.Driver;
using static LanguageExt.Prelude;

namespace DistributedPiCollectorWs.Services;

public static class StaticHelper
{
    public static MongoClientSettings CreateMongoConnection(string? connectionString, string? userName,
        string? password)
    {
        var cs = MongoClientSettings.FromConnectionString(connectionString);
        if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
            return cs;
        cs.Credential = MongoCredential.CreateCredential("admin", userName, password);
        return cs;
    }

    public static Option<T> AsOption<T>(this T? value) => Optional(value);

    public static string GenerateObjectKey(int iteration, int precision) => $"{iteration}-{precision}";

    public static IEnumerable<Task<TOut>> SelectAsync<TIn, TOut>(this IEnumerable<TIn> source,
        Func<TIn, Task<TOut>> selector, int parallelism, CancellationToken cancellationToken)
    {
        var semaphoreSlim = new SemaphoreSlim(parallelism);
        return source.Select(async element =>
        {
            await semaphoreSlim.WaitAsync(cancellationToken);
            try
            {
                return await selector.Invoke(element);
            }
            finally
            {
                semaphoreSlim.Release();
            }
        });
    }

    public static Dictionary<MinioConnectionName, MinioConfiguration>
        LoadMinioConfigurationFromEnvironment(IConfigurationHandler configurationHandler)
    {
        var connections = new Dictionary<MinioConnectionName, MinioConfiguration>();

        var mainConfig = new MinioConfiguration
        {
            AccessKey = configurationHandler.GetValue<string>("MINIO_MAIN_ACCESS_KEY"),
            SecretKey = configurationHandler.GetValue<string>("MINIO_MAIN_SECRET_KEY"),
            Host = configurationHandler.GetValue<string>("MINIO_MAIN_HOST"),
        };

        var factorialConfig = new MinioConfiguration
        {
            AccessKey = configurationHandler.GetValue<string>("MINIO_FACTORIAL_ACCESS_KEY"),
            SecretKey = configurationHandler.GetValue<string>("MINIO_FACTORIAL_SECRET_KEY"),
            Host = configurationHandler.GetValue<string>("MINIO_FACTORIAL_HOST"),
        };
        
        connections[MinioConnectionName.MainConnection] =  mainConfig;
        connections[MinioConnectionName.FactorialConnection] = factorialConfig;
        
        return connections;
    }
}