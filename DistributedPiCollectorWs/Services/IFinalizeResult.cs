﻿using DistributedPiCollectorWs.Services.Tos;

namespace DistributedPiCollectorWs.Services;

public interface IFinalizeResult
{
    ValueTask<FinalizeResultTo> CalculateFinalResultAsync(Guid sessionId, CancellationToken cancellationToken);
}