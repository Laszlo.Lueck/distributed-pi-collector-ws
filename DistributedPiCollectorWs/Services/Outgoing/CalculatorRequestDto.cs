using System.Text.Json.Serialization;

namespace DistributedPiCollectorWs.Services.Outgoing;

public record CalculatorRequestDto(
    [property: JsonPropertyName("id"), JsonInclude]
    int Id,
    [property: JsonPropertyName("precision"), JsonInclude]
    int Precision,
    [property: JsonPropertyName("session"), JsonInclude]
    Guid Session);