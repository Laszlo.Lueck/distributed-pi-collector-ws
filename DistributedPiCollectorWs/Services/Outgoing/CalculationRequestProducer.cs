using Silverback.Messaging.Publishing;

namespace DistributedPiCollectorWs.Services.Outgoing;

public class CalculationRequestProducer(ILogger<CalculationRequestProducer> logger, IServiceScopeFactory scopeFactory) : ICalculationRequestProducer
{
    public Task PublishAsync(CalculatorRequestDto requestDto)
    {
        try
        {
            return Task.Run(async () =>
            {
                using var scope = scopeFactory.CreateScope();
                var publisher = scope.ServiceProvider.GetRequiredService<IPublisher>();
                logger.LogInformation("send request to calculator-slaves with id {Id}", requestDto.Id);
                await publisher.PublishAsync(requestDto);
            });
        }
        catch (Exception e)
        {
            logger.LogError(e, "an error occured");
            return Task.FromException(e);
            
        }
    }
}
