namespace DistributedPiCollectorWs.Services.Outgoing;

public interface ICalculationRequestProducer
{
    Task PublishAsync(CalculatorRequestDto requestDto);
}