using System.Text.Json;
using DistributedPiCollectorWs.Connections;
using DistributedPiCollectorWs.Endpoints.ListMeasurementPartTimings;
using DistributedPiCollectorWs.Services.Storage;
using DistributedPiCollectorWs.Services.Tos;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DistributedPiCollectorWs.Services;

public class ListMeasurementPartTimingsService(
    ILogger<ListMeasurementPartTimingsService> logger,
    IDatabaseConnector<MeasurementPartDocument> dbPartConnector) : IListMeasurementPartTimings
{
    public async Task<IEnumerable<ListMeasurementPartTimingTo>> GetListMeasurementPartTimingsAsync(
        GetListMeasurementPartTimingsRequest request)
    {
        logger.LogInformation("list measurement part timings with filter {Filter}", JsonSerializer.Serialize(request));
        var filter = Builders<MeasurementPartDocument>.Filter
            .Eq(d => d.Session, request.SessionId);

        var totalCount = await dbPartConnector.MongoCollection.CountDocumentsAsync(filter);

        if (totalCount <= 1000)
        {
            return (await dbPartConnector
                    .MongoCollection
                    .Find(filter)
                    .Sort(Builders<MeasurementPartDocument>.Sort.Ascending(d => d.Iteration))
                    .ToListAsync())
                .Select(d => new ListMeasurementPartTimingTo(d.Iteration, d.ProcessTimeMs, d.ResultSize, d.HostName,
                    d.ProcessTimeMs is 0 || d.ResultSize is 0 ? "Unprocessed" : "Aggregated"));
        }

        var bucketSize = (int)Math.Ceiling((double)totalCount / 1000);

        logger.LogInformation("calculated bucket size {BucketSize}", bucketSize);

        var aggregation = await dbPartConnector.MongoCollection
            .Aggregate()
            .Match(filter)
            .Sort(Builders<MeasurementPartDocument>.Sort.Ascending(d => d.Iteration))
            .Group(new BsonDocument
            {
                {
                    "_id",
                    new BsonDocument("$toInt", new BsonDocument("$divide", new BsonArray { "$iteration", bucketSize }))
                },
                { "MinCalculationTime", new BsonDocument("$min", "$processTimeMs") },
                { "MaxCalculationTime", new BsonDocument("$max", "$processTimeMs") },
                { "MinResultSize", new BsonDocument("$min", "$resultSize") },
                { "MaxResultSize", new BsonDocument("$max", "$resultSize") },
                { "FirstIteration", new BsonDocument("$first", "$iteration") },
                { "HostName", new BsonDocument("$first", "$hostName") },
                {
                    "CountNonZero", new BsonDocument("$sum", new BsonDocument("$cond", new BsonArray
                    {
                        new BsonDocument("$gt", new BsonArray { "$processTimeMs", 0 }), 1, 0
                    }))
                }
            })
            .ToListAsync();

        logger.LogInformation("received {Count} measurement part timings", aggregation.Count);

        return aggregation
            .Select(d =>
                new ListMeasurementPartTimingTo(
                    d.GetValue("FirstIteration", new BsonInt32(0)).AsInt32,
                    d.GetValue("MaxCalculationTime", new BsonDouble(0)).AsDouble,
                    d.GetValue("MaxResultSize", new BsonInt64(0)).AsInt64,
                    d.GetValue("HostName", new BsonString("")).AsString,
                    d["CountNonZero"].AsInt32 == bucketSize ? "Aggregated" :
                    d["CountNonZero"].AsInt32 == 0 ? "Unprocessed" : "Partial"
                ))
            .OrderBy(d => d.Id);
    }
}