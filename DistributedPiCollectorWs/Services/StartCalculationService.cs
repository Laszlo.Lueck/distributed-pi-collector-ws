﻿using DistributedPiCollectorWs.Calculation;
using DistributedPiCollectorWs.Connections;
using DistributedPiCollectorWs.Services.Storage;
using DistributedPiCollectorWs.Services.Tos;
using MongoDB.Driver;

namespace DistributedPiCollectorWs.Services;

public class StartCalculationService(
    ILogger<StartCalculationService> logger,
    ICalculatorStarter calculatorStarter,
    IDatabaseConnector<MeasurementDocument> dbConnector,
    IDatabaseConnector<MeasurementPartDocument> databasePartConnector,
    IServiceProvider serviceProvider) : IStartCalculation
{
    private readonly IStorageCacheAsync _partRegistry =
        serviceProvider.GetRequiredKeyedService<IStorageCacheAsync>(MinioBucketName.PartBucket.Name);


    public async Task StartCalculationAsync(StartCalculationTo startCalculationTo, CancellationToken cancellationToken)
    {
        cancellationToken.ThrowIfCancellationRequested();
        var filter = Builders<MeasurementDocument>
            .Filter
            .And(
                Builders<MeasurementDocument>.Filter.Eq(d => d.Session, startCalculationTo.SessionId),
                Builders<MeasurementDocument>.Filter.Eq(d => d.ExecState, ExecState.New));
        var entryOpt = await dbConnector.FindOneAsync(filter);

        await entryOpt.Match(
            Some: async result =>
            {
                logger.LogInformation("found document for session {SessionId}", startCalculationTo.SessionId);
                //first some checks.
                if (startCalculationTo.EndInclusiveValue > result.Iterations)
                {
                    logger.LogWarning("EndInclusiveValue {EndInclusiveValue} is greater than iterations {Iterations}",
                        startCalculationTo.EndInclusiveValue, result.Iterations);
                    return;
                }

                var endInclusive = startCalculationTo.EndInclusiveValue == -1
                    ? result.Iterations
                    : startCalculationTo.EndInclusiveValue;

                await Parallel.ForEachAsync(
                    Enumerable
                        .Range(startCalculationTo.StartValue, endInclusive)
                        .Chunk(1000),
                    new ParallelOptions { MaxDegreeOfParallelism = 5, CancellationToken = cancellationToken },
                    async (grp, ct) =>
                    {
                        ct.ThrowIfCancellationRequested();
                        var tasks = grp
                            .SelectAsync(
                                async tpl => await CheckCacheAndCreatePartDocument(tpl, result, _partRegistry), 10,
                                cancellationToken);

                        var docs = await Task.WhenAll(tasks);

                        await databasePartConnector.InsertManyAsync(docs);

                        await Parallel.ForEachAsync(docs.Where(d => d.ReturnValueHash == string.Empty),
                            new ParallelOptions { MaxDegreeOfParallelism = 10, CancellationToken = cancellationToken },
                            async (element, xt) =>
                            {
                                xt.ThrowIfCancellationRequested();
                                await calculatorStarter.AddEntryToProducer(element.Iteration, result.Precision,
                                    element.Session);
                            });
                    });

                var update = Builders<MeasurementDocument>
                    .Update
                    .Set(d => d.ExecState, ExecState.Executing);
                await dbConnector.UpdateOneAsync(filter, update);
            },
            None: async () =>
            {
                await Task.Run(() => logger.LogWarning(
                    "Kein Eintrag für SessionId {SessionId} gefunden oder Berechnung wurde bereits gestartet",
                    startCalculationTo.SessionId), cancellationToken);
            });
    }

    private static readonly Func<Guid, int, string, string, long, MeasurementPartDocument> CreateDocument =
        (session, iteration, returnValueHash, hostName, resultSize) =>
            new MeasurementPartDocument(session, iteration, returnValueHash, 0, hostName, resultSize);

    private static readonly Func<int, MeasurementDocument, IStorageCacheAsync,
            Task<MeasurementPartDocument>>
        CheckCacheAndCreatePartDocument =
            async (tpl, result, minioPartResults) =>
            {
                var key = StaticHelper.GenerateObjectKey(tpl, result.Precision);
                var cacheOpt = await minioPartResults.GetItemFromCacheAsync(key);
                return cacheOpt
                    .Match(
                        cacheResult => CreateDocument(result.Session, tpl, key, "cached", cacheResult.LongLength),
                        () => CreateDocument(result.Session, tpl, string.Empty, string.Empty, 0));
            };
}