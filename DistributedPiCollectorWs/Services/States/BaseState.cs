﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace DistributedPiCollectorWs.Services.States;

public class BaseState
{
    [JsonPropertyName("state"), JsonInclude]
    private readonly int _state;
    
    [JsonPropertyName("stateText"), JsonInclude]
    private readonly string _stateText;
    
    
    public override string ToString() => JsonSerializer.Serialize(this);
    
    public int GetState() => _state;

    public string GetText() => _stateText;
    
    protected BaseState(int state, string stateText)
    {
        _state = state;
        _stateText = stateText;
    }
}