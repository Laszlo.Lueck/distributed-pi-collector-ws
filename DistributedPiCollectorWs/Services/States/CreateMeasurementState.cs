namespace DistributedPiCollectorWs.Services.States;

public class CreateMeasurementState:BaseState
{
    private CreateMeasurementState(int state, string stateText) : base(state, stateText)
    {
    }


    public static readonly CreateMeasurementState Ok = new(200, "Ok");

    public static readonly CreateMeasurementState PrecisionTooLowError =
        new(400, "precision too low for amount of iterations");
}