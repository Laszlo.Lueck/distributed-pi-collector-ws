﻿namespace DistributedPiCollectorWs.Services.States;

public class FinalizeResultState : BaseState
{
    private FinalizeResultState(int state, string stateText) : base(state, stateText)
    {
    }


    public static readonly FinalizeResultState Ok = new(200, "Ok");

    public static readonly FinalizeResultState Error = new(400,
        "Something went wrong during calculation, see additionalText for details");
    
    public static readonly FinalizeResultState NotFound = new(404, "Session not found");
    
    public static readonly FinalizeResultState SessionAlreadyFinished = new(409, "Session already finished");
    
    public static readonly FinalizeResultState SessionNotCompleted = new(410, "Session not completed");
}