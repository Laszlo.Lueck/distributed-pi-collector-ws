using DistributedPiCollectorWs.Services.Tos;

namespace DistributedPiCollectorWs.Services;

public interface IMeasurementDistribution
{
    Task<ListMeasurementDistributionTo> GetMeasurementDistributionAsync(Guid sessionId);
}