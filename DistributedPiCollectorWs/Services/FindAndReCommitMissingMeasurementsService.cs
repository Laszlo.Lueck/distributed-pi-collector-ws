﻿using System.Collections.Concurrent;
using DistributedPiCollectorWs.Connections;
using DistributedPiCollectorWs.Services.Outgoing;
using DistributedPiCollectorWs.Services.Storage;
using MongoDB.Driver;

namespace DistributedPiCollectorWs.Services;

public class FindAndReCommitMissingMeasurementsService(
    ILogger<FindAndReCommitMissingMeasurementsService> logger,
    IDatabaseConnector<MeasurementDocument> dbConnector,
    ICalculationRequestProducer producer,
    IServiceProvider serviceProvider) : IFindAndReCommitMissingMeasurements
{
    private readonly IStorageCacheAsync _partRegistry =
        serviceProvider.GetRequiredKeyedService<IStorageCacheAsync>(MinioBucketName.PartBucket.Name);


    public async Task FindAndReCommitMissingMeasurementsAsync(Guid sessionId)
    {
        var ft = Builders<MeasurementDocument>.Filter.Eq(d => d.Session, sessionId);
        var dbEntry = await dbConnector.FindOneAsync(ft);
        await dbEntry.Match(async result =>
        {
            logger.LogInformation(
                "found document for session {SessionId} with precision {Precision} and iterations {Iterations}",
                sessionId, result.Precision, result.Iterations);
            await Parallel.ForEachAsync(Enumerable.Range(1, result.Iterations),
                new ParallelOptions { MaxDegreeOfParallelism = 4 }, async (value, b) =>
                {
                    var objName = $"{value}-{result.Precision}";
                    var res = await _partRegistry.IsKeyExistAsync(objName);
                    if (!res)
                    {
                        logger.LogInformation("create new build request for {objName}", objName);
                        var dto = new CalculatorRequestDto(value, result.Precision, result.Session);
                        await producer.PublishAsync(dto);
                    }
                });
        }, async () => await Task.Run(() => logger.LogWarning("no document found for session {SessionId}", sessionId)));
    }
}