using Microsoft.Extensions.Diagnostics.HealthChecks;
using MongoDB.Driver;

namespace DistributedPiCollectorWs.Services;

public class CustomMongoHealthCheck(MongoClientSettings mongoClientSettings, string databaseName) : IHealthCheck
{
    public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context,
        CancellationToken cancellationToken = default)
    {
        try
        {
            var mongoClient = new MongoClient(mongoClientSettings);
            if (string.IsNullOrEmpty(databaseName)) return await Task.FromResult(HealthCheckResult.Healthy("connection to MongoDb established"));
            var databaseNames = await mongoClient.ListDatabaseNamesAsync(cancellationToken);
            var dbsList = await databaseNames.ToListAsync(cancellationToken);
            return !dbsList.Contains(databaseName)
                ? HealthCheckResult.Unhealthy($"Datenbank {databaseName} not found")
                : HealthCheckResult.Healthy($"Connection to MongoDb established and database {databaseName} found");
        }
        catch (Exception ex)
        {
            return new HealthCheckResult(context.Registration.FailureStatus, exception: ex);
        }
    }
}