﻿using System.Numerics;
using DistributedPiCollectorWs.Connections;
using DistributedPiCollectorWs.Services.States;
using DistributedPiCollectorWs.Services.Storage;
using DistributedPiCollectorWs.Services.Tos;
using LanguageExt;
using LanguageExt.UnsafeValueAccess;
using MongoDB.Driver;

namespace DistributedPiCollectorWs.Services;

public class FinalizeResult(
    ILogger<FinalizeResult> logger,
    ICalculateFinalResult calculateFinalResultService,
    IDatabaseConnector<MeasurementDocument> dbConnector,
    IDatabaseConnector<MeasurementPartDocument> databasePartConnector,
    IServiceProvider serviceProvider) : IFinalizeResult
{
    private readonly IStorageCacheAsync _mainRegistry =
        serviceProvider.GetRequiredKeyedService<IStorageCacheAsync>(MinioBucketName.ResultBucket.Name);

    private readonly IStorageCacheAsync _partRegistry =
        serviceProvider.GetRequiredKeyedService<IStorageCacheAsync>(MinioBucketName.PartBucket.Name);


    public async ValueTask<FinalizeResultTo> CalculateFinalResultAsync(Guid sessionId,
        CancellationToken cancellationToken)
    {
        logger.LogInformation("calculate final result for session {SessionId}", sessionId);

        return await CheckIfSessionExistsAsync(sessionId, dbConnector)
            .MapAsync(async either =>
            {
                return await either.MatchAsync(
                    RightAsync: async document =>
                    {
                        return await CheckIfFinalizingAlreadyDone(document).MatchAsync(
                            RightAsync: async innerDocument =>
                            {
                                var c2 = await CheckIfSessionIsCompletedAsync(sessionId, databasePartConnector);
                                return await c2.MatchAsync<FinalizeResultTo>(
                                    RightAsync: async _ => await FinalizeAndUpdateCalculation(logger, dbConnector,
                                        databasePartConnector, calculateFinalResultService, _mainRegistry,
                                        _partRegistry, sessionId,
                                        innerDocument, cancellationToken),
                                    LeftAsync: async innerInnerState => await AddLeftLeg(logger,
                                        $"session with id {sessionId} is not completed", innerInnerState, sessionId));
                            },
                            LeftAsync: async innerState => await AddLeftLeg(logger,
                                $"document with session {sessionId} id already finalized", innerState, sessionId));
                    },
                    LeftAsync: async state =>
                        await AddLeftLeg(logger, $"no session with id {sessionId} in database", state, sessionId));
            });
    }

    private static readonly Func<ILogger, string, FinalizeResultState, Guid, ValueTask<FinalizeResultTo>> AddLeftLeg =
        async (logger, text, state, sessionId) =>
        {
            logger.LogWarning("{Message}", text);
            return await new ValueTask<FinalizeResultTo>(
                new FinalizeResultTo(string.Empty, text, 0, state, sessionId));
        };

    private static readonly Func<ILogger, IDatabaseConnector<MeasurementDocument>,
            IDatabaseConnector<MeasurementPartDocument>, ICalculateFinalResult, IStorageCacheAsync,
            IStorageCacheAsync, Guid,
            MeasurementDocument, CancellationToken,
            ValueTask<FinalizeResultTo>>
        FinalizeAndUpdateCalculation =
            async (logger, dbConnector, databasePartConnector, calculateFinalResultService, minioResults,
                minioPartResults, sessionId,
                innerDocument, ct) =>
            {
                logger.LogInformation("try to calculate final result for session {SessionId}",
                    sessionId);

                var finalResultOpt = await FinalCalculatePiAsync(sessionId, databasePartConnector,
                    calculateFinalResultService,
                    innerDocument, logger, minioPartResults, ct);

                var finalResult = finalResultOpt.Match(
                    Some: result => result,
                    None: () => new CalculateFinalResultTo(BigInteger.Zero, 0.0)
                );

                await minioResults.SetItemToCacheAsync(sessionId.ToString(), finalResult.Value.ToByteArray());

                var ud = Builders<MeasurementDocument>.Update;
                var update = ud
                    .Set(d => d.Finalized, true)
                    .Set(d => d.ExecState, ExecState.Finished)
                    .Set(d => d.FinalizedTimeMs, finalResult.CalculationTimeMs);
                var fd = Builders<MeasurementDocument>
                    .Filter
                    .Eq(d => d.Session, sessionId);
                await dbConnector.UpdateOneAsync(fd, update);


                var outVal = new FinalizeResultTo(finalResult.Value.ToString(), string.Empty,
                    finalResult.CalculationTimeMs,
                    FinalizeResultState.Ok, sessionId);
                return outVal;
            };

#nullable disable
    private static readonly
        Func<Guid, IDatabaseConnector<MeasurementPartDocument>, ICalculateFinalResult, MeasurementDocument, ILogger,
            IStorageCacheAsync, CancellationToken,
            ValueTask<Option<CalculateFinalResultTo>>> FinalCalculatePiAsync =
            async (sessionId, databasePartConnector, calculateFinalResultService, document, logger, minioPartResults,
                ct) =>
            {
                logger.LogInformation("build filter, projection and sort for session {SessionId}", sessionId);
                var fd = Builders<MeasurementPartDocument>.Filter.Eq(d => d.Session, sessionId);

                var filteredAsync = databasePartConnector
                    .MongoCollection
                    .Find(fd)
                    .Project(d => d.ReturnValueHash)
                    .ToEnumerable()
                    .SelectAsync(async d => await minioPartResults.GetItemFromCacheAsync(d), 4, ct);

                var filteredOpts = await Task.WhenAll(filteredAsync);

                var filteredResults = filteredOpts
                    .Somes()
                    .Map(btArray => new BigInteger(btArray)) ?? [];

                return await calculateFinalResultService.FinalCalculatePi(filteredResults, document.Precision);
            };
#nullable restore


    private static readonly
        Func<Guid, IDatabaseConnector<MeasurementPartDocument>, ValueTask<Either<FinalizeResultState, Unit>>>
        CheckIfSessionIsCompletedAsync =
            async (sessionId, databasePartConnector) =>
            {
                var fd = Builders<MeasurementPartDocument>.Filter;
                var filter = fd.And(
                    fd.Eq(e => e.ReturnValueHash, string.Empty),
                    fd.Eq(e => e.Session, sessionId)
                );

                var res = await databasePartConnector.MongoCollection.CountDocumentsAsync(filter);
                return res > 0 ? FinalizeResultState.SessionNotCompleted : Unit.Default;
            };

    private static readonly Func<MeasurementDocument, Either<FinalizeResultState, MeasurementDocument>>
        CheckIfFinalizingAlreadyDone =
            document => document.Finalized ? FinalizeResultState.SessionAlreadyFinished : document;

    private static readonly Func<Guid, IDatabaseConnector<MeasurementDocument>,
            ValueTask<Either<FinalizeResultState, MeasurementDocument>>>
        CheckIfSessionExistsAsync = async (sessionId, dbConnector) =>
        {
            var fd = Builders<MeasurementDocument>.Filter.Eq(e => e.Session, sessionId);
            var res = await dbConnector.FindOneAsync(fd);
            if (res.IsNone)
                return FinalizeResultState.NotFound;

            return res.ValueUnsafe();
        };
}