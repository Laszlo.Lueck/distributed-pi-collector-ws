using System.Numerics;
using DistributedPiCollectorWs.Services.Tos;
using LanguageExt;

namespace DistributedPiCollectorWs.Services;

public interface ICalculateFinalResult
{
    Task<Option<CalculateFinalResultTo>> FinalCalculatePi(IEnumerable<BigInteger> source, int precision);
}