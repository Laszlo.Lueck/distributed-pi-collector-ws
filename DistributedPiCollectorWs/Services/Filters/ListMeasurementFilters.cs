using DistributedPiCollectorWs.Endpoints.ListMeasurements;

namespace DistributedPiCollectorWs.Services.Filters;

public record ListMeasurementFilters(SortOrder SortOrder, int Page, int PageSize)
{
    public static implicit operator ListMeasurementFilters(ListMeasurementsFilterRequest request) =>
        new(SortOrder.FromId(request.SortOrder), request.Page, request.PageSize);
}