using System.Text.Json;
using DistributedPiCollectorWs.Connections;
using DistributedPiCollectorWs.Services.Filters;
using DistributedPiCollectorWs.Services.Storage;
using DistributedPiCollectorWs.Services.Tos;
using MongoDB.Driver;

namespace DistributedPiCollectorWs.Services;

public class ListMeasurementsService(
    ILogger<ListMeasurementsService> logger,
    IDatabaseConnector<MeasurementDocument> dbConnector,
    IDatabaseConnector<MeasurementPartDocument> dbPartConnector) : IListMeasurements
{
    public async Task<ListMeasurementTo> GetListMeasurementsAsync(ListMeasurementFilters request, CancellationToken cancellationToken)
    {
        logger.LogInformation("list measurements with filter {Filter}", JsonSerializer.Serialize(request));
        var sd = Builders<MeasurementDocument>.Sort;
        var sortDefinition = request.SortOrder.GetId() switch
        {
            0 => sd.Ascending(d => d.CreateDate),
            1 => sd.Descending(d => d.CreateDate),
            _ => sd.Ascending(d => d.CreateDate)
        };

        var filter = Builders<MeasurementDocument>.Filter.Empty;

        var countFacet = AggregateFacet.Create("count",
            PipelineDefinition<MeasurementDocument, AggregateCountResult>.Create([
                PipelineStageDefinitionBuilder.Count<MeasurementDocument>()
            ]));

        var dataFacet = AggregateFacet.Create("data",
            PipelineDefinition<MeasurementDocument, MeasurementDocument>.Create([
                PipelineStageDefinitionBuilder.Sort(sortDefinition),
                PipelineStageDefinitionBuilder.Skip<MeasurementDocument>((request.Page - 1) * request.PageSize),
                PipelineStageDefinitionBuilder.Limit<MeasurementDocument>(request.PageSize)
            ]));

        var aggregation = await dbConnector
            .MongoCollection
            .Aggregate()
            .Match(filter)
            .Facet(countFacet, dataFacet)
            .ToListAsync(cancellationToken: cancellationToken);

        var dc = aggregation
            .First()
            .Facets
            .First(x => x.Name == "count")
            .Output<AggregateCountResult>();


        var count = dc is not null && dc.Count > 0 ? dc[0]?.Count ?? 0 : 0;


        var totalPages = (int)Math.Ceiling((double)count / request.PageSize);

        logger.LogInformation("received {Count} measurements", count);
        logger.LogInformation("get with page {Page} and page size {PageSize}", request.Page, request.PageSize);
        logger.LogInformation("calculate total pages {TotalPages}", totalPages);

        var data = aggregation
            .First()
            .Facets
            .First(x => x.Name == "data")
            .Output<MeasurementDocument>();

        var extendedMeasurementsAsync = data.SelectAsync(async d =>
        {

                var matchStage =
                    PipelineStageDefinitionBuilder.Match<MeasurementPartDocument>(e => e.Session == d.Session);
                var countStage = PipelineStageDefinitionBuilder.Count<MeasurementPartDocument>();
                var pipelineStages = new IPipelineStageDefinition[] { matchStage, countStage };

                var partFacet = AggregateFacet.Create("totalParts",
                    PipelineDefinition<MeasurementPartDocument, AggregateCountResult>.Create(pipelineStages));


                var nonEmptyMatchStage =
                    PipelineStageDefinitionBuilder.Match<MeasurementPartDocument>(e =>
                        !string.IsNullOrEmpty(e.ReturnValueHash));
                var filteredPipelineStages = new IPipelineStageDefinition[]
                    { matchStage, nonEmptyMatchStage, countStage };

                var filteredParts = AggregateFacet.Create("filteredParts",
                    PipelineDefinition<MeasurementPartDocument, AggregateCountResult>.Create(filteredPipelineStages));

                var partAggregation = await dbPartConnector
                    .MongoCollection
                    .Aggregate()
                    .Facet(partFacet, filteredParts)
                    .ToListAsync(cancellationToken: cancellationToken);

                var totalParts = partAggregation
                    .FirstOrDefault()
                    .AsOption()
                    .Bind(f => f.Facets.FirstOrDefault(x => x.Name == "totalParts").AsOption())
                    .Bind(t => t.Output<AggregateCountResult>().AsOption())
                    .Bind(o => o.ElementAtOrDefault(0).AsOption())
                    .Map(r => r.Count)
                    .IfNone(0);

                var filteredPartsCount = partAggregation
                    .FirstOrDefault()
                    .AsOption()
                    .Bind(f => f.Facets.FirstOrDefault(x => x.Name == "filteredParts").AsOption())
                    .Bind(t => t.Output<AggregateCountResult>().AsOption())
                    .Bind(o => o.ElementAtOrDefault(0).AsOption())
                    .Map(r => r.Count)
                    .IfNone(0);

                logger.LogInformation("calculate total parts {TotalParts} and filtered parts {FilteredParts}",
                    totalParts,
                    filteredPartsCount);

                d.TotalParts = totalParts;
                d.TotalFinishedParts = filteredPartsCount;
                return d;
        }, 4, cancellationToken);

        var dataList = await Task.WhenAll(extendedMeasurementsAsync);

        return new ListMeasurementTo(totalPages, count) { Data = dataList };
    }
}