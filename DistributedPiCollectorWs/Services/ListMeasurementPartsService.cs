using System.Numerics;
using System.Text.Json;
using DistributedPiCollectorWs.Connections;
using DistributedPiCollectorWs.Endpoints.ListMeasurementParts;
using DistributedPiCollectorWs.Services.Storage;
using DistributedPiCollectorWs.Services.Tos;
using MongoDB.Driver;

namespace DistributedPiCollectorWs.Services;

public class ListMeasurementPartsService(
    ILogger<ListMeasurementPartsService> logger,
    IDatabaseConnector<MeasurementPartDocument> dbPartConnector,
    IServiceProvider serviceProvider) : IListMeasurementParts
{
    private readonly IStorageCacheAsync _partRegistry =
        serviceProvider.GetRequiredKeyedService<IStorageCacheAsync>(MinioBucketName.PartBucket.Name);

    public async Task<ListMeasurementPartsTo> GetListMeasurementPartsAsync(GetListMeasurementPartsRequest request,
        CancellationToken cancellationToken)
    {
        logger.LogInformation("list measurement parts with filter {Filter}", JsonSerializer.Serialize(request));

        var sd = Builders<MeasurementPartDocument>.Sort.Ascending(d => d.Iteration);
        var filter = Builders<MeasurementPartDocument>
            .Filter
            .Eq(d => d.Session, request.SessionId);

        var countFacet = AggregateFacet.Create("count",
            PipelineDefinition<MeasurementPartDocument, AggregateCountResult>.Create([
                PipelineStageDefinitionBuilder.Count<MeasurementPartDocument>()
            ]));

        var dataFacet = AggregateFacet.Create("data",
            PipelineDefinition<MeasurementPartDocument, MeasurementPartDocument>.Create([
                PipelineStageDefinitionBuilder.Sort(sd),
                PipelineStageDefinitionBuilder.Skip<MeasurementPartDocument>((request.Page - 1) * request.PageSize),
                PipelineStageDefinitionBuilder.Limit<MeasurementPartDocument>(request.PageSize)
            ]));


        var aggregation = await dbPartConnector
            .MongoCollection
            .Aggregate()
            .Match(filter)
            .Facet(countFacet, dataFacet)
            .ToListAsync(cancellationToken: cancellationToken);

        var ca = aggregation
            .First()
            .Facets
            .First(x => x.Name == "count")
            .Output<AggregateCountResult>();

        var count = ca is not null && ca.Count > 0 ? ca[0]?.Count ?? 0 : 0;

        var totalPages = (int)Math.Ceiling((double)count / request.PageSize);


        logger.LogInformation("received {Count} measurement parts", count);
        logger.LogInformation("get with page {Page} and page size {PageSize}", request.Page, request.PageSize);
        logger.LogInformation("calculate total pages {TotalPages}", totalPages);

        var measurementPartListAsync = aggregation
            .First()
            .Facets
            .First(x => x.Name == "data")
            .Output<MeasurementPartDocument>()
            .SelectAsync(async d =>
            {
                var osElementOpt = await _partRegistry.GetItemFromCacheAsync(d.ReturnValueHash);
                ListMeasurementPartTo ret = d;

                ret.Result = osElementOpt
                    .Match(data => new BigInteger(data ?? throw new ArgumentNullException(nameof(data))).ToString(),
                        () => $"not entry found for key {d.ReturnValueHash}");
                return ret;
            }, 4, cancellationToken);

        var measurementPartList = await Task.WhenAll(measurementPartListAsync);

        return new ListMeasurementPartsTo(request.SessionId, measurementPartList, totalPages, count);
    }
}