using System.Text.Json.Serialization;

namespace DistributedPiCollectorWs.Services;

public partial class SortOrder
{
    [JsonPropertyName("id"), JsonInclude]
    private int _id;
    [JsonPropertyName("order"), JsonInclude]
    private string? _order;
    
    private SortOrder(int id, string? order)
    {
        _order = order;
        _id = id;
    }

    public override string? ToString() => _order;
    
    public int GetId() => _id;

    public static SortOrder FromId(int id)
    {
        return id switch
        {
            0 => Ascending,
            1 => Descending,
            _ => throw new ArgumentOutOfRangeException(nameof(id), id, null)

        };
    }

    public static SortOrder Ascending => new SortOrder(0, "asc");
    public static SortOrder Descending => new SortOrder(1, "desc");
}

public partial class SortOrder
{
    public SortOrder(){} 
}