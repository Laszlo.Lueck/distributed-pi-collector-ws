using DistributedPiCollectorWs.Services.Filters;
using DistributedPiCollectorWs.Services.Tos;

namespace DistributedPiCollectorWs.Services;

public interface IListMeasurements
{
    Task<ListMeasurementTo> GetListMeasurementsAsync(ListMeasurementFilters request, CancellationToken cancellationToken);
}