using LanguageExt;

namespace DistributedPiCollectorWs.Services;

public interface IGetResultBySessionId
{
    
    Task<Option<string>> GetResultBySessionIdAsync(Guid sessionId);
    
}