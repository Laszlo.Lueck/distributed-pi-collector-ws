using System.Diagnostics;
using System.Numerics;
using DistributedPiCollectorWs.Calculation;
using DistributedPiCollectorWs.Services.Tos;
using LanguageExt;

namespace DistributedPiCollectorWs.Services;

public class CalculateFinalResultService(ILogger<CalculateFinalResultService> logger) : ICalculateFinalResult
{
    public async Task<Option<CalculateFinalResultTo>> FinalCalculatePi(IEnumerable<BigInteger> source, int precision)
    {
        return await Task.Run(() =>
        {
            var sw = Stopwatch.StartNew();
            BigInteger twelve = new(12);
            BigInteger precisionExtended = BigInteger.Pow(10, precision);
            var sqrRootTerm = BigInteger.Pow(640320, 3) * BigInteger.Pow(precisionExtended, 2);
            var aggregator = source.Aggregate(new BigInteger(0), (acc, val) => acc + val);
            var value = BigInteger.Pow(precisionExtended, 2) /
                        (twelve * aggregator * precisionExtended / sqrRootTerm.NewtonPlusSqrt());
            sw.Stop();
            logger.LogInformation("calculate pi-decimals in {CalculationTimeMs} ms", sw.Elapsed.TotalMilliseconds);

            return new CalculateFinalResultTo(value, sw.Elapsed.TotalMilliseconds);
        });
    }
}