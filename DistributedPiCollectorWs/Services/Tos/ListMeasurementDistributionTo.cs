namespace DistributedPiCollectorWs.Services.Tos;

public record ListMeasurementDistributionTo(Guid SessionId, List<MeasurementDistributionTo> Measurements);

public record MeasurementDistributionTo(List<MeasurementEntry> ProcessTimes, double TotalProcessTime, string HostName);

public record MeasurementEntry(int Iteration, double ProcessTimeMs);