using DistributedPiCollectorWs.Services.States;

namespace DistributedPiCollectorWs.Services.Tos;

public record CreateMeasurementServiceResultTo(CreateMeasurementState State, string AdditionalText, Guid SessionId);