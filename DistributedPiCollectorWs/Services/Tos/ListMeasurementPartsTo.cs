namespace DistributedPiCollectorWs.Services.Tos;

public record ListMeasurementPartsTo(
    Guid SessionId,
    IEnumerable<ListMeasurementPartTo> MeasurementPartToList,
    int PageCount,
    long DocCount);