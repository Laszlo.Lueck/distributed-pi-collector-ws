using DistributedPiCollectorWs.Endpoints.CreateMeasurement;

namespace DistributedPiCollectorWs.Services.Tos;

public record CreateMeasurementServiceTo(int Precision, int Iterations)
{
    public static implicit operator CreateMeasurementServiceTo(CreateMeasurementRequest from) =>
        new(from.Precision, from.Iterations);
};