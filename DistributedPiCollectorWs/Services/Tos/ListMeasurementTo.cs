using System.Text.Json.Serialization;
using DistributedPiCollectorWs.Services.Storage;

namespace DistributedPiCollectorWs.Services.Tos;

public record ListMeasurementTo(int TotalPages, long TotalDocumentCount)
{
    [JsonPropertyName("data"), JsonInclude]
    public IEnumerable<MeasurementDocument> Data { get; set; } = [];
}