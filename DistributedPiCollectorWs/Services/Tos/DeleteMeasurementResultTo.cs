namespace DistributedPiCollectorWs.Services.Tos;

public record DeleteMeasurementResultTo(Guid SessionId, bool MeasurementDeleteSuccess, bool MeasurementPartDeleteSuccess, long MeasurementPartDeletedCount);