using DistributedPiCollectorWs.Services.Storage;

namespace DistributedPiCollectorWs.Services.Tos;

public record ListMeasurementPartTo(
    int Id,
    double CalculationTimeMs,
    string HostName)
{
    public string? Result { get; set; }
    
    public static implicit operator ListMeasurementPartTo(MeasurementPartDocument from) =>
        new(from.Iteration, from.ProcessTimeMs, from.HostName);
};