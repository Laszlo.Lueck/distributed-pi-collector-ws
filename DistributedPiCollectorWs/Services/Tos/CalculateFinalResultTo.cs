
using System.Numerics;

namespace DistributedPiCollectorWs.Services.Tos;

public record CalculateFinalResultTo(BigInteger Value, double CalculationTimeMs);