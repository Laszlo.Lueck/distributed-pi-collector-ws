﻿using DistributedPiCollectorWs.Endpoints.StartCalculation;

namespace DistributedPiCollectorWs.Services.Tos;

public record StartCalculationTo(Guid SessionId, int StartValue, int EndInclusiveValue)
{
    public static implicit operator StartCalculationTo(StartCalculationRequest from) =>
        new(from.SessionId, from.StartValue, from.EndInclusiveValue);
};