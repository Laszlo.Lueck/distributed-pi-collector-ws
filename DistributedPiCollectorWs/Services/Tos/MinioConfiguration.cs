﻿namespace DistributedPiCollectorWs.Services.Tos;

public class MinioConfiguration
{
    public string Host { get; set; } = null!;
    public string AccessKey { get; set; } = null!;
    public string SecretKey { get; set; } = null!;
}