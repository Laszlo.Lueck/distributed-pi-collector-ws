﻿using DistributedPiCollectorWs.Services.States;

namespace DistributedPiCollectorWs.Services.Tos;

public record FinalizeResultTo(
    string Result,
    string AdditionalText,
    double CalculationTimeMs,
    FinalizeResultState State,
    Guid SessionId);