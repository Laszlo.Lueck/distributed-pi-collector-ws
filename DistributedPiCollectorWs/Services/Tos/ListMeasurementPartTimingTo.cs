namespace DistributedPiCollectorWs.Services.Tos;

public record ListMeasurementPartTimingTo(int Id, double CalculationTimeMs, long ResultSize, string HostName, string State);