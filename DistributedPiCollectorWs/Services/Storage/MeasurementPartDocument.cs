using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DistributedPiCollectorWs.Services.Storage;

public record MeasurementPartDocument(
    [property: BsonElement("session"), BsonGuidRepresentation(GuidRepresentation.Standard)] Guid Session,
    [property: BsonElement("iteration")] int Iteration,
    [property: BsonElement("returnValueHash")] string ReturnValueHash,
    [property: BsonElement("processTimeMs")] double ProcessTimeMs,
    [property: BsonElement("hostName")] string HostName,
    [property: BsonElement("resultSize")] long ResultSize
)
{
    [BsonId, BsonRepresentation(BsonType.ObjectId)]
    public ObjectId Id { get; set; }
};