using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DistributedPiCollectorWs.Services.Storage;

public record MeasurementDocument(
    [property: BsonElement("session"), BsonGuidRepresentation(GuidRepresentation.Standard)] Guid Session,
    [property: BsonElement("createDate")] DateTime CreateDate,
    [property: BsonElement("iterations")] int Iterations,
    [property: BsonElement("precision")] int Precision,
    [property: BsonElement("finalized")] bool Finalized,
    [property: BsonElement("finalizedTimeMs")] double FinalizedTimeMs,
    [property: BsonElement("checked")] bool Checked,
    [property: BsonElement("execState"), BsonRepresentation(BsonType.String)] ExecState ExecState
)
{
    [BsonId, BsonRepresentation(BsonType.ObjectId)]
    public ObjectId Id { get; set; }

    [BsonElement("totalParts")] public long TotalParts { get; set; }

    [BsonElement("totalFinishedParts")] public long TotalFinishedParts { get; set; }
    
}