namespace DistributedPiCollectorWs.Services.Storage;

public enum ExecState
{
    New = 0,
    Executing = 1,
    Finished = 2
}