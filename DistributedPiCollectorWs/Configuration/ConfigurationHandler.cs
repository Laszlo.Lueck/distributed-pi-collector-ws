namespace DistributedPiCollectorWs.Configuration;

public class ConfigurationHandler(IConfiguration configuration) : IConfigurationHandler
{
    public T GetValue<T>(string key)
    {
        return configuration.GetValue<T>(key) ??
               throw new ArgumentNullException($"value for key {key} cannot be found in configuration");
    }
}