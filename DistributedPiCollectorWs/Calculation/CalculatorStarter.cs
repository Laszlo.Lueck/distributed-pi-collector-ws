using DistributedPiCollectorWs.Services.Outgoing;

namespace DistributedPiCollectorWs.Calculation;

public class CalculatorStarter(ICalculationRequestProducer producer) : ICalculatorStarter
{
    public async Task<CalculatorRequestDto> AddEntryToProducer(int value, int precision, Guid sessionId)
    {
        var req = new CalculatorRequestDto(Id: value, Precision: precision, Session: sessionId);
        await producer.PublishAsync(req);
        return req;
    }
}