using System.Numerics;
using DistributedPiCollectorWs.Connections;

namespace DistributedPiCollectorWs.Calculation;

public interface ICalculatorTest
{
    Task<bool> CheckPrecisionLengthAsync(int iterations, int precision);
}

public class CalculatorTest(ILogger<CalculatorTest> logger, IServiceProvider serviceProvider)
    : ICalculatorTest
{
    private static readonly BigInteger ConstA = new(13591409);
    private static readonly BigInteger ConstB = new(545140134);
    private static readonly BigInteger ConstC = new(-640320);

    private readonly IStorageCacheAsync _baseStorageCacheMinio =
        serviceProvider.GetRequiredKeyedService<IStorageCacheAsync>(MinioBucketName.FactorialBucket.Name);

    public async Task<bool> CheckPrecisionLengthAsync(int iterations, int precision)
    {
        logger.LogInformation("calculate async {Iteratee}", iterations);
        var f1 = await Factorial(iterations);
        var f3 = await Factorial(3 * iterations);
        var f6 = await Factorial(6 * iterations);
        BigInteger bi = new(iterations);
        var factor = BigInteger.Pow(10, precision);

        var ret = factor * f6 * (ConstA + ConstB * bi) /
                  (f3 * BigInteger.Pow(f1, 3) *
                   BigInteger.Pow(ConstC, 3 * iterations));

        return ret != BigInteger.Zero;
    }


    private static IEnumerable<int> GenerateIntSequence(int startValue, int endInclusive)
    {
        for (var i = startValue; i <= endInclusive; i++)
            yield return i;
    }

    private async Task<BigInteger> Factorial(int source)
    {
        var itemOpt = await _baseStorageCacheMinio.GetItemFromCacheAsync($"fac_{source}");
        return itemOpt.Match(item => new BigInteger(item), () => _factorial(source));
    }

    private readonly Func<int, BigInteger> _factorial = source =>
        GenerateIntSequence(1, source)
            .AsParallel()
            .Aggregate(BigInteger.One, (acc, val) => acc * val);
}