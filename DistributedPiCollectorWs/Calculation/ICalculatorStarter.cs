using DistributedPiCollectorWs.Services.Outgoing;

namespace DistributedPiCollectorWs.Calculation;

public interface ICalculatorStarter
{
    Task<CalculatorRequestDto> AddEntryToProducer(int value, int precision, Guid sessionId);
}