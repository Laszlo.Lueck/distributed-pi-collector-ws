using System.Numerics;

namespace DistributedPiCollectorWs.Calculation;

public static class SqrtHelper
{
    public static BigInteger NewtonPlusSqrt(this BigInteger x)
    {
        if (x < 144838757784765629)
        {
            uint vInt = (uint)Math.Sqrt((ulong)x);
            if (x >= 4503599761588224 && (ulong)vInt * vInt > (ulong)x)
            {
                vInt--;
            }

            return vInt;
        }

        double xAsDub = (double)x;
        if (xAsDub < 8.5e37)
        {
            ulong vInt = (ulong)Math.Sqrt(xAsDub);
            BigInteger v = (vInt + (ulong)(x / vInt)) >> 1;
            return v * v <= x ? v : v - 1;
        }

        if (xAsDub < 4.3322e127)
        {
            BigInteger v = (BigInteger)Math.Sqrt(xAsDub);
            v = (v + x / v) >> 1;
            if (xAsDub > 2e63)
            {
                v = (v + x / v) >> 1;
            }

            return v * v <= x ? v : v - 1;
        }

        int xLen = GetBitLengthFallback(x);
        int wantedPrecision = (xLen + 1) / 2;
        int xLenMod = xLen + (xLen & 1) + 1;

        long tempX = (long)(x >> (xLenMod - 63));
        double tempSqrt1 = Math.Sqrt(tempX);
        ulong valLong = (ulong)BitConverter.DoubleToInt64Bits(tempSqrt1) & 0x1fffffffffffffL;
        if (valLong == 0)
        {
            valLong = 1UL << 53;
        }

        ////////  Classic Newton Iterations ////////
        BigInteger val = ((BigInteger)valLong << 52) + (x >> xLenMod - 3 * 53) / valLong;
        int size = 106;
        for (; size < 256; size <<= 1)
        {
            val = (val << (size - 1)) + (x >> xLenMod - 3 * size) / val;
        }

        if (xAsDub > 4e254) // 4e254 = 1<<845.76973610139
        {
            int numOfNewtonSteps = BitOperations.Log2((uint)(wantedPrecision / size)) + 2;
            int wantedSize = (wantedPrecision >> numOfNewtonSteps) + 2;
            int needToShiftBy = size - wantedSize;
            val >>= needToShiftBy;
            size = wantedSize;
            do
            {
                int shiftX = xLenMod - 3 * size;
                BigInteger valSqrd = (val * val) << (size - 1);
                BigInteger valSu = (x >> shiftX) - valSqrd;
                val = (val << size) + valSu / val;
                size *= 2;
            } while (size < wantedPrecision);
        }

        int oversidedBy = size - wantedPrecision;
        BigInteger saveDroppedDigitsBi = val & ((BigInteger.One << oversidedBy) - 1);
        int downby = oversidedBy < 64 ? (oversidedBy >> 2) + 1 : oversidedBy - 32;
        ulong saveDroppedDigits = (ulong)(saveDroppedDigitsBi >> downby);
        
        val >>= oversidedBy;

        if (saveDroppedDigits == 0 && val * val > x)
        {
            val--;
        }

        return val;
    }

    private static int GetBitLengthFallback(BigInteger x) // only support x > 0
    {
        byte[] bytes = x.ToByteArray();
        byte msb = bytes[^1];
        int msbBits = 0;
        while (msb != 0)
        {
            msb >>= 1;
            msbBits++;
        }

        return (bytes.Length - 1) * 8 + msbBits;
    }
}