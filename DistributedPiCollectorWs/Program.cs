using DistributedPiCollectorWs;
using DistributedPiCollectorWs.Calculation;
using DistributedPiCollectorWs.Configuration;
using DistributedPiCollectorWs.Connections;
using DistributedPiCollectorWs.Services;
using DistributedPiCollectorWs.Services.Incoming;
using DistributedPiCollectorWs.Services.Outgoing;
using DistributedPiCollectorWs.Services.Storage;
using FastEndpoints;
using FastEndpoints.OpenTelemetry;
using FastEndpoints.OpenTelemetry.Middleware;
using FastEndpoints.Swagger;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using OpenTelemetry.Exporter;
using OpenTelemetry.Logs;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;
using SimpleMinioClient;
using SimpleMinioClient.SimpleMinioClientHealthCheck;

const string myAllowSpecificOrigins = "_myAllowSpecificOrigins";

var builder = WebApplication.CreateBuilder(args);
BsonSerializer.RegisterSerializer(new GuidSerializer(GuidRepresentation.Standard));
builder
    .Services
    .AddFastEndpoints()
    .SwaggerDocument();

builder
    .Logging
    .ClearProviders();

var rb = ResourceBuilder
    .CreateDefault()
    .AddService(serviceName: builder.Environment.ApplicationName);

builder
    .Logging
    .AddOpenTelemetry(options =>
    {
        options.IncludeScopes = true;
        options.IncludeFormattedMessage = true;
        options.SetResourceBuilder(rb);
        options
            .AddOtlpExporter(a =>
            {
                a.Endpoint = new Uri("http://seq.localdomain:8192/ingest/otlp/v1/logs");
                a.Headers = "X-Seq-ApiKey=9crNn9HqqWGIdL4BQyYI";
                a.Protocol = OtlpExportProtocol.HttpProtobuf;
            });
        if (builder.Environment.IsDevelopment())
            options.AddConsoleExporter();

    });

builder
    .Services
    .AddOpenTelemetry()
    .WithTracing(tcb =>
    {
        tcb
            .SetResourceBuilder(rb)
            .AddSource(builder.Environment.ApplicationName)
            .AddAspNetCoreInstrumentation()
            .AddHttpClientInstrumentation()
            .AddMongoDBInstrumentation()
            .AddFastEndpointsInstrumentation()
            .AddOtlpExporter(a =>
            {
                a.Endpoint = new Uri("http://seq.localdomain:8192/ingest/otlp/v1/traces");
                a.Headers = "X-Seq-ApiKey=QeRoPXistPJdSlQe8Nrv";
                a.Protocol = OtlpExportProtocol.HttpProtobuf;
            });
        if (builder.Environment.IsDevelopment())
            tcb.AddConsoleExporter();
    });

IConfigurationHandler configurationHandler = new ConfigurationHandler(builder.Configuration);

builder.Services.AddMinioFromEnvironment(configurationHandler);

builder.Services.AddSingleton(configurationHandler);
builder.Services.Configure<MongoStorageMeasurementsDatabaseSettings>(d =>
{
    d.DatabaseName = configurationHandler.GetValue<string>("MONGO_DATABASE_NAME");
    d.CollectionName = configurationHandler.GetValue<string>("MONGO_COLLECTION_MEASUREMENTS_NAME");
    d.ConnectionString = configurationHandler.GetValue<string>("MONGO_CONNECTION_STRING");
    d.UserName = configurationHandler.GetValue<string>("MONGO_USERNAME");
    d.Password = configurationHandler.GetValue<string>("MONGO_PASSWORD");
});
builder.Services.Configure<MongoStorageMeasurementPartsDatabaseSettings>(d =>
{
    d.DatabaseName = configurationHandler.GetValue<string>("MONGO_DATABASE_NAME");
    d.ConnectionString = configurationHandler.GetValue<string>("MONGO_CONNECTION_STRING");
    d.CollectionName = configurationHandler.GetValue<string>("MONGO_COLLECTION_MEASUREMENT_PARTS_NAME");
    d.UserName = configurationHandler.GetValue<string>("MONGO_USERNAME");
    d.Password = configurationHandler.GetValue<string>("MONGO_PASSWORD");
});

builder
    .Services
    .AddSingleton<IDatabaseConnector<MeasurementDocument>, MongoDbMeasurementConnector<MeasurementDocument>>();

builder
    .Services
    .AddSingleton<IDatabaseConnector<MeasurementPartDocument>,
        MongoDbMeasurementPartConnector<MeasurementPartDocument>>();

builder.Services.AddAuthorization();
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: myAllowSpecificOrigins,
        policy =>
        {
            policy
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader();
        });
});

builder.Services.AddSingleton<ICalculationRequestProducer, CalculationRequestProducer>();

builder.Services.AddSingleton<ICalculatorTest, CalculatorTest>();
builder.Services.AddSingleton<ICalculateFinalResult, CalculateFinalResultService>();
builder.Services.AddSingleton<IFinalizeResult, FinalizeResult>();
builder.Services.AddSingleton<IListMeasurements, ListMeasurementsService>();
builder.Services.AddSingleton<IGetResultBySessionId, GetResultBySessionIdService>();
builder.Services.AddSingleton<IDeleteMeasurement, DeleteMeasurementService>();
builder.Services.AddSingleton<IListMeasurementParts, ListMeasurementPartsService>();
builder.Services.AddSingleton<IListMeasurementPartTimings, ListMeasurementPartTimingsService>();
builder.Services.AddSingleton<IMeasurementDistribution, MeasurementDistributionService>();
builder.Services.AddSingleton<IFindAndReCommitMissingMeasurements, FindAndReCommitMissingMeasurementsService>();

builder
    .Services
    .AddSilverback()
    .UseModel()
    .WithConnectionToMessageBroker(o => o.AddKafka())
    .AddEndpointsConfigurator<CalculationMiddlewareEndpointConfigurator>()
    .AddSingletonSubscriber<CalculationResponseSubscriber>();

builder.Services.AddSingleton(new CustomMongoHealthCheck(StaticHelper.CreateMongoConnection(
    configurationHandler.GetValue<string>("MONGO_CONNECTION_STRING"),
    configurationHandler.GetValue<string>("MONGO_USERNAME"),
    configurationHandler.GetValue<string>("MONGO_PASSWORD")), "PiDatabase"));

builder.Services.AddEndpointsApiExplorer();
builder
    .Services
    .AddHealthChecks()
    .AddCheck<CustomMongoHealthCheck>("MongoDbHealthCheck")
    .AddSimpleMinioReadynessCheck(f => f.GetRequiredService<ISimpleMinioClient>())
    .AddKafka(config => config.BootstrapServers = configurationHandler.GetValue<string>("KAFKA_URL"),
        failureStatus: HealthStatus.Unhealthy, timeout: TimeSpan.FromSeconds(4), name: "KafkaHealthCheck");

builder.Services.AddScoped<ICalculatorStarter, CalculatorStarter>();
builder.Services.AddScoped<IStartCalculation, StartCalculationService>();
builder.Services.AddScoped<ICreateMeasurement, CreateMeasurement>();


var app = builder.Build();

app.MapHealthChecks("/alive", new HealthCheckOptions
{
    Predicate = check => check.Tags.Contains("live")
});
app.MapHealthChecks("/healthz", new HealthCheckOptions
{
    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
});

app.UseHttpsRedirection();
app.UseAuthorization();
app.UseCors(x =>
    x
        .AllowAnyMethod()
        .AllowAnyHeader()
        .SetIsOriginAllowed(_ => true)
        .AllowAnyOrigin()
);

app.UseFastEndpoints().UseSwaggerGen();
app.Logger.StartingApp();
app.UseFastEndpointsDiagnosticsMiddleware();

await app.RunAsync();

namespace DistributedPiCollectorWs
{
    public static partial class ApplicationLogs
    {
        [LoggerMessage(1, LogLevel.Information, "Starting the app...")]
        public static partial void StartingApp(this ILogger logger);
    }
}