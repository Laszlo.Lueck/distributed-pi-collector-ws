using FastEndpoints;
using FluentValidation;

namespace DistributedPiCollectorWs.Endpoints.ListMeasurementParts;

public class ListMeasurementPartsRequestValidator: Validator<GetListMeasurementPartsRequest>
{

    public ListMeasurementPartsRequestValidator()
    {
        RuleFor(request => request.Page)
            .NotNull()
            .WithMessage("Page is required")
            .GreaterThan(0)
            .WithMessage("Page must be greater than 0");

        RuleFor(request => request.PageSize)
            .NotNull()
            .WithMessage("Page size is required")
            .GreaterThan(0)
            .WithMessage("Page size must be greater than 0");
        
    }
    

    
    
}