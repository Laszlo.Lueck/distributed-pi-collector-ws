using DistributedPiCollectorWs.Services.Tos;

namespace DistributedPiCollectorWs.Endpoints.ListMeasurementParts;

public record GetListMeasurementPartsResponse(
    Guid SessionId,
    IEnumerable<GetListMeasurementPart> MeasurementPartList,
    int PageCount,
    long DocCount
)
{
    
    public static implicit operator GetListMeasurementPartsResponse(ListMeasurementPartsTo to) =>
        new(
            to.SessionId,
            to.MeasurementPartToList.Select(x => (GetListMeasurementPart) x),
            to.PageCount,
            to.DocCount
        );
    
};