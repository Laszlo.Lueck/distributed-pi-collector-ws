using System.Text.Json;
using DistributedPiCollectorWs.Services;
using FastEndpoints;

namespace DistributedPiCollectorWs.Endpoints.ListMeasurementParts;

public class GetListMeasurementPartsEndpoint(
    ILogger<GetListMeasurementPartsEndpoint> logger,
    IListMeasurementParts listMeasurementParts)
    : Endpoint<GetListMeasurementPartsRequest, GetListMeasurementPartsResponse>
{
    public override void Configure()
    {
        Get("calculation/parts/list/{SessionId}/{Page}/{PageSize}");
        AllowAnonymous();
        Validator<ListMeasurementPartsRequestValidator>();
    }

    public override async Task HandleAsync(GetListMeasurementPartsRequest req, CancellationToken ct)
    {
        logger.LogInformation("list measurement parts with filter {Filter}", JsonSerializer.Serialize(req));
        GetListMeasurementPartsResponse result = await listMeasurementParts.GetListMeasurementPartsAsync(req, ct);
        await SendOkAsync(result, ct);
    }
}