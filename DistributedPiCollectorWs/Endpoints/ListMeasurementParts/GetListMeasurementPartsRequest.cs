namespace DistributedPiCollectorWs.Endpoints.ListMeasurementParts;

public record GetListMeasurementPartsRequest(
    Guid SessionId,
    int Page,
    int PageSize
);