using DistributedPiCollectorWs.Services.Tos;

namespace DistributedPiCollectorWs.Endpoints.ListMeasurementParts;

public record GetListMeasurementPart(
    int Id,
    string? Result,
    double CalculationTimeMs
)
{
    public static implicit operator GetListMeasurementPart(ListMeasurementPartTo to) =>
        new(
            to.Id,
            to.Result,
            to.CalculationTimeMs
        );
};