﻿namespace DistributedPiCollectorWs.Endpoints.FinalizeResult;

public record FinalizeResultRequest(Guid SessionId);