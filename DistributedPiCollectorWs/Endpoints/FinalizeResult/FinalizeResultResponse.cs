﻿using DistributedPiCollectorWs.Services.Tos;

namespace DistributedPiCollectorWs.Endpoints.FinalizeResult;

public record FinalizeResultResponse(
    Guid Session,
    string Result,
    string AdditionalText,
    double CalculationTimeMs,
    int State
    )
{
    public static implicit operator FinalizeResultResponse(FinalizeResultTo from) =>
        new(from.SessionId, from.Result, from.AdditionalText, from.CalculationTimeMs, from.State.GetState());
};