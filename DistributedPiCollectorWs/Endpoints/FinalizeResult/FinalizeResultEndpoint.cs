﻿using DistributedPiCollectorWs.Services;
using FastEndpoints;

namespace DistributedPiCollectorWs.Endpoints.FinalizeResult;

public class FinalizeResultEndpoint(
    ILogger<FinalizeResultEndpoint> logger,
    IFinalizeResult finalizeResult)
    : Endpoint<FinalizeResultRequest, FinalizeResultResponse>
{
    public override void Configure()
    {
        Put("calculation/finalize");
        AllowAnonymous();
    }

    public override async Task HandleAsync(FinalizeResultRequest req, CancellationToken ct)
    {
        logger.LogInformation("finalize calculation for session {Session}", req.SessionId.ToString());
        var result = await finalizeResult.CalculateFinalResultAsync(req.SessionId, ct);
        FinalizeResultResponse ret = result;

        switch (result.State.GetState())
        {
            case 400:
                await SendAsync(ret, StatusCodes.Status400BadRequest, ct);
                break;
            case 404:
                await SendAsync(ret, StatusCodes.Status404NotFound, ct);
                break;
            default:
                await SendOkAsync(ret, ct);
                break;
        }
    }
}