
using DistributedPiCollectorWs.Services;
using FastEndpoints;

namespace DistributedPiCollectorWs.Endpoints.CreateMeasurement;

public class CreateMeasurementEndpoint(
    ILogger<CreateMeasurementEndpoint> logger,
    ICreateMeasurement createMeasurement)
    : Endpoint<CreateMeasurementRequest, CreateMeasurementResponse>
{
    public override void Configure()
    {
        Put("calculation/create");
        AllowAnonymous();
        Validator<CreateMeasurementRequestValidator>();
    }
    
    public override async Task HandleAsync(CreateMeasurementRequest req, CancellationToken ct)
    {
        var result = await createMeasurement.CreateMeasurementAsync(req);
        logger.LogInformation("create measurement with state {Id}", result.State.GetState());

        CreateMeasurementResponse returnValue = result;      

        await SendOkAsync(returnValue, ct);
    }
}