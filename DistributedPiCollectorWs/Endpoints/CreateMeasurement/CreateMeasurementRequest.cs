namespace DistributedPiCollectorWs.Endpoints.CreateMeasurement;

public record CreateMeasurementRequest(
    int Iterations,
    int Precision);