using FastEndpoints;
using FluentValidation;

namespace DistributedPiCollectorWs.Endpoints.CreateMeasurement;

public class CreateMeasurementRequestValidator : Validator<CreateMeasurementRequest>
{
    public CreateMeasurementRequestValidator()
    {
        RuleFor(request => request.Iterations)
            .NotNull()
            .WithMessage("Iterations is required")
            .GreaterThan(0)
            .WithMessage("Iterations must be greater than 0");
        
        RuleFor(request => request.Precision)
            .NotNull()
            .WithMessage("Precision is required")
            .GreaterThan(0)
            .WithMessage("Precision must be greater than 0");
        
    }
}