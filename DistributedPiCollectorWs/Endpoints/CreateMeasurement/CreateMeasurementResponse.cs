using DistributedPiCollectorWs.Services.Tos;

namespace DistributedPiCollectorWs.Endpoints.CreateMeasurement;

public record CreateMeasurementResponse(
    string StateText,
    string AdditionalText,
    Guid SessionId,
    int State)
{
    public static implicit operator CreateMeasurementResponse(CreateMeasurementServiceResultTo from) =>
        new(from.State.GetText(), from.AdditionalText, from.SessionId, from.State.GetState());
}