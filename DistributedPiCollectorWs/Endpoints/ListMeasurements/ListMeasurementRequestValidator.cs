using FastEndpoints;
using FluentValidation;

namespace DistributedPiCollectorWs.Endpoints.ListMeasurements;

public class ListMeasurementRequestValidator : Validator<ListMeasurementsFilterRequest>
{
    public ListMeasurementRequestValidator()
    {
        RuleFor(request => request.SortOrder)
            .NotNull()
            .WithMessage("Sort order is required")
            .Must(v => v is 0 or 1)
            .WithMessage("Sort order must be 0 or 1");

        RuleFor(request => request.Page)
            .NotNull()
            .WithMessage("Page is required")
            .GreaterThan(0)
            .WithMessage("Page must be greater than 0");
        
        RuleFor(request => request.PageSize)
            .NotNull()
            .WithMessage("Page size is required")
            .GreaterThan(0)
            .WithMessage("Page size must be greater than 0");
    }
}