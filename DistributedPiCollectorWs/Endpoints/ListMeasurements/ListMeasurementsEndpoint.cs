using System.Text.Json;
using DistributedPiCollectorWs.Services;
using FastEndpoints;

namespace DistributedPiCollectorWs.Endpoints.ListMeasurements;

public class ListMeasurementsEndpoint(ILogger<ListMeasurementsEndpoint> logger, IListMeasurements listMeasurements)
    : Endpoint<ListMeasurementsFilterRequest,ListMeasurementResponse>
{
    public override void Configure()
    {
        Get("calculation/list/{SortOrder}/{Page}/{PageSize}");
        AllowAnonymous();
        Validator<ListMeasurementRequestValidator>();
    }

    public override async Task HandleAsync(ListMeasurementsFilterRequest req, CancellationToken ct)
    {
        logger.LogInformation("list measurements with filter {Filter}", JsonSerializer.Serialize(req));
        ListMeasurementResponse result = await listMeasurements.GetListMeasurementsAsync(req, ct);
        await SendOkAsync(result, ct);
    }
}