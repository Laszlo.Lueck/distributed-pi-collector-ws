namespace DistributedPiCollectorWs.Endpoints.ListMeasurements;

public record ListMeasurementsFilterRequest(
    int SortOrder,
    int Page,
    int PageSize
);