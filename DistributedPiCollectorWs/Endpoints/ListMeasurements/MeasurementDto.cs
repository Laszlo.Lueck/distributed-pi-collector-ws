using DistributedPiCollectorWs.Services.Storage;

namespace DistributedPiCollectorWs.Endpoints.ListMeasurements;

public record MeasurementDto(
    Guid Id,
    DateTime CreateDate,
    int Iterations,
    int Precision,
    bool Checked,
    bool Finalized,
    double FinalizedTimeMs,
    long TotalParts,
    long TotalFinishedParts,
    string ExecState)
{
    public static explicit operator MeasurementDto(MeasurementDocument document) => new(Id: document.Session,
        CreateDate: document.CreateDate, Iterations: document.Iterations, Precision: document.Precision,
        Checked: document.Checked, Finalized: document.Finalized,
        FinalizedTimeMs: document.FinalizedTimeMs, TotalParts: document.TotalParts,
        TotalFinishedParts: document.TotalFinishedParts, ExecState: document.ExecState.ToString());
}