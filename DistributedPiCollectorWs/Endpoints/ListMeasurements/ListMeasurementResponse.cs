using DistributedPiCollectorWs.Services.Tos;

namespace DistributedPiCollectorWs.Endpoints.ListMeasurements;

public record ListMeasurementResponse(
    IEnumerable<MeasurementDto> MeasurementList,
    int PageCount,
    long DocCount
    
    
)
{
    public static implicit operator ListMeasurementResponse(ListMeasurementTo from) =>
        new(from.Data.Select(f => (MeasurementDto)f), from.TotalPages, from.TotalDocumentCount);
}