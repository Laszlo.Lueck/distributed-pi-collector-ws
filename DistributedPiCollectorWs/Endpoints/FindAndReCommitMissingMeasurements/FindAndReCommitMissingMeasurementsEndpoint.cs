﻿using DistributedPiCollectorWs.Services;
using FastEndpoints;

namespace DistributedPiCollectorWs.Endpoints.FindAndReCommitMissingMeasurements;

public class FindAndReCommitMissingMeasurementsEndpoint(
    ILogger<FindAndReCommitMissingMeasurementsEndpoint> logger,
    IFindAndReCommitMissingMeasurements findAndReCommitMissingMeasurements)
    : Endpoint<FindAndReCommitMissingMeasurementsRequest,
        FindAndReCommitMissingMeasurementsResponse>
{
    public override void Configure()
    {
        Post("calculation/find-and-recommit/{SessionId}");
        AllowAnonymous();
    }

    public override async Task HandleAsync(FindAndReCommitMissingMeasurementsRequest req, CancellationToken ct)
    {
        logger.LogInformation("find and recommit missing measurements for session {SessionId}",
            req.SessionId.ToString());
        _ = findAndReCommitMissingMeasurements.FindAndReCommitMissingMeasurementsAsync(req.SessionId);
        await SendOkAsync(new FindAndReCommitMissingMeasurementsResponse(req.SessionId), ct);
    }
}