using DistributedPiCollectorWs.Services;
using FastEndpoints;

namespace DistributedPiCollectorWs.Endpoints.MeasurementDistribution;

public class MeasurementDistributionEndpoint(
    ILogger<MeasurementDistributionEndpoint> logger,
    IMeasurementDistribution measurementDistributionService)
    : Endpoint<MeasurementDistributionRequest, MeasurementDistributionResponse>
{
    public override void Configure()
    {
        Get("calculation/distribution/{SessionId}");
        AllowAnonymous();
    }


    public override async Task HandleAsync(MeasurementDistributionRequest req, CancellationToken ct)
    {
        logger.LogInformation("get measurement distribution for session {SessionId}", req.SessionId.ToString());
        var result = await measurementDistributionService.GetMeasurementDistributionAsync(req.SessionId);
        await SendOkAsync(result, ct);
    }
}