using DistributedPiCollectorWs.Services.Tos;

namespace DistributedPiCollectorWs.Endpoints.MeasurementDistribution;

public record MeasurementDistributionResponse(Guid SessionId, List<MeasurementDistributionRecord> Measurements)
{
    public static implicit operator MeasurementDistributionResponse(ListMeasurementDistributionTo from) =>
        new(from.SessionId, from.Measurements.Select(d => (MeasurementDistributionRecord)d).ToList());
}

public record MeasurementDistributionRecord(List<MeasurementEntry> ProcessTimes, double TotalProcessTime, string HostName)
{
    public static implicit operator MeasurementDistributionRecord(MeasurementDistributionTo from) =>
        new(from.ProcessTimes, from.TotalProcessTime, from.HostName);
};