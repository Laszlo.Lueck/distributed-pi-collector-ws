﻿namespace DistributedPiCollectorWs.Endpoints.StartCalculation;

public record StartCalculationResponse(Guid SessionId);