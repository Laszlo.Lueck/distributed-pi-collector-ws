﻿namespace DistributedPiCollectorWs.Endpoints.StartCalculation;

public record StartCalculationRequest(Guid SessionId, int StartValue, int EndInclusiveValue);