﻿using DistributedPiCollectorWs.Services;
using FastEndpoints;

namespace DistributedPiCollectorWs.Endpoints.StartCalculation;

public class StartCalculationEndpoint(
    ILogger<StartCalculationEndpoint> logger,
    IStartCalculation startCalculationService)
    : Endpoint<StartCalculationRequest, StartCalculationResponse>
{
    public override void Configure()
    {
        Post("calculation/start");
        AllowAnonymous();
    }

    public override async Task HandleAsync(StartCalculationRequest request,
        CancellationToken cancellationToken)
    {
        logger.LogInformation("starting calculation for {SessionId} as fire and forget", request.SessionId);
        _ = startCalculationService.StartCalculationAsync(request, cancellationToken);
        await SendOkAsync(new StartCalculationResponse(request.SessionId), cancellationToken);
    }
}