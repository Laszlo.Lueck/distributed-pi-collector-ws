using DistributedPiCollectorWs.Services;
using FastEndpoints;

namespace DistributedPiCollectorWs.Endpoints.ResultBySessionId;

public class GetResultBySessionIdEndpoint(
    ILogger<GetResultBySessionIdEndpoint> logger,
    IGetResultBySessionId getResultBySessionIdService)
    : Endpoint<GetResultBySessionIdRequest, GetResultBySessionIdResponse>
{
    public override void Configure()
    {
        Get("calculation/result/{SessionId}");
        AllowAnonymous();
    }
    
    public override async Task HandleAsync(GetResultBySessionIdRequest req,
        CancellationToken ct)
    {
        logger.LogInformation("get result by session id {SessionId}", req.SessionId);
        var resultOpt = await getResultBySessionIdService.GetResultBySessionIdAsync(req.SessionId);

        await resultOpt.Match
        (
            Some: result => SendOkAsync(new GetResultBySessionIdResponse(result), ct),
            None: () => SendNotFoundAsync(ct)
        );
    }

}