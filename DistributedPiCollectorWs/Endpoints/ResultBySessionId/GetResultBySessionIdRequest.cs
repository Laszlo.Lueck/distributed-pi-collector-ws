namespace DistributedPiCollectorWs.Endpoints.ResultBySessionId;

public record GetResultBySessionIdRequest(
    Guid SessionId
);