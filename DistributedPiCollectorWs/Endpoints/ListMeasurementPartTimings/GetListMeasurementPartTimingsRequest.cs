namespace DistributedPiCollectorWs.Endpoints.ListMeasurementPartTimings;

public record GetListMeasurementPartTimingsRequest(
    Guid SessionId
);