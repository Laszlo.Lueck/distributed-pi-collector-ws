using System.Text.Json;
using DistributedPiCollectorWs.Services;
using DistributedPiCollectorWs.Services.Tos;
using FastEndpoints;

namespace DistributedPiCollectorWs.Endpoints.ListMeasurementPartTimings;

public class GetListMeasurementPartTimingsEndpoint(
    ILogger<GetListMeasurementPartTimingsEndpoint> logger,
    IListMeasurementPartTimings listMeasurementPartTimings)
    : Endpoint<GetListMeasurementPartTimingsRequest, GetListMeasurementPartTimingsResponse>
{
    public override void Configure()
    {
        Get("calculation/parts/timings/list/{SessionId}");
        AllowAnonymous();
    }

    public override async Task HandleAsync(GetListMeasurementPartTimingsRequest req, CancellationToken ct)
    {
        logger.LogInformation("list measurement part timings with filter {Filter}", JsonSerializer.Serialize(req));
        IEnumerable<ListMeasurementPartTimingTo> result =
            await listMeasurementPartTimings.GetListMeasurementPartTimingsAsync(req);

        var ret = result.Select(d => (GetListMeasurementPartTimingEntry)d);
        
        await SendOkAsync(new GetListMeasurementPartTimingsResponse(ret), ct);
    }
}