using DistributedPiCollectorWs.Services.Tos;

namespace DistributedPiCollectorWs.Endpoints.ListMeasurementPartTimings;

public record GetListMeasurementPartTimingEntry(int Id, double CalculationTimeMs, long ResultSize, string HostName, string State)
{
    public static implicit operator GetListMeasurementPartTimingEntry(ListMeasurementPartTimingTo from) =>
        new(from.Id, from.CalculationTimeMs, from.ResultSize, from.HostName, from.State);
};