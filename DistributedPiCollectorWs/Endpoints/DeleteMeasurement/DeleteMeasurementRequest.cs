namespace DistributedPiCollectorWs.Endpoints.DeleteMeasurement;

public record DeleteMeasurementRequest(Guid SessionId);
