using DistributedPiCollectorWs.Services;
using FastEndpoints;

namespace DistributedPiCollectorWs.Endpoints.DeleteMeasurement;

public class DeleteMeasurementEndpoint(
    ILogger<DeleteMeasurementEndpoint> logger,
    IDeleteMeasurement deleteMeasurementService) : Endpoint<DeleteMeasurementRequest, DeleteMeasurementResult>
{
    public override void Configure()
    {
        Delete("calculation/delete/{SessionId}");
        AllowAnonymous();
    }

    public override async Task HandleAsync(DeleteMeasurementRequest req, CancellationToken ct)
    {
        logger.LogInformation("remove measurement with session {SessionId}", req.SessionId.ToString());
        DeleteMeasurementResult deleteResult = await deleteMeasurementService.DeleteMeasurementAsync(req.SessionId);

        await SendOkAsync(deleteResult, ct);
    }
}