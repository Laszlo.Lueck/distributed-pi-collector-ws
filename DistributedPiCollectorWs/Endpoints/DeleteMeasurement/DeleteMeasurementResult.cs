using DistributedPiCollectorWs.Services.Tos;

namespace DistributedPiCollectorWs.Endpoints.DeleteMeasurement;

public record DeleteMeasurementResult(
    Guid SessionId,
    bool MeasurementDeleteSuccess,
    bool MeasurementPartDeleteSuccess,
    long MeasurementPartDeletedCount)
{
    public static implicit operator DeleteMeasurementResult(DeleteMeasurementResultTo from) => new(
        SessionId: from.SessionId, MeasurementDeleteSuccess: from.MeasurementDeleteSuccess,
        MeasurementPartDeleteSuccess: from.MeasurementPartDeleteSuccess,
        MeasurementPartDeletedCount: from.MeasurementPartDeletedCount);
};