﻿using System.Text.Json;
using DistributedPiCollectorWs.Services;

namespace DistributedPiCollectorWsTest;


public class SortOrderTests
{
    [Test]
    public async Task FromId_WithValidId_ReturnsCorrectSortOrder()
    {
        var ascending = SortOrder.FromId(0);
        await Assert.That(ascending.GetId()).IsEqualTo(0);
        await Assert.That(ascending.ToString()).IsEqualTo("asc");

        var descending = SortOrder.FromId(1);
        await Assert.That(descending.GetId()).IsEqualTo(1);
        await Assert.That(descending.ToString()).IsEqualTo("desc");
    }

    [Test]
    public void FromId_WithInvalidId_ThrowsArgumentOutOfRangeException()
    {
        Assert.Throws<ArgumentOutOfRangeException>(() => SortOrder.FromId(-1));
        Assert.Throws<ArgumentOutOfRangeException>(() => SortOrder.FromId(2));
    }

    [Test]
    public async Task GetId_ReturnsCorrectId()
    {
        await Assert.That(SortOrder.Ascending.GetId()).IsEqualTo(0);
        await Assert.That(SortOrder.Descending.GetId()).IsEqualTo(1);
    }

    [Test]
    public async Task ToString_ReturnsCorrectString()
    {
        await Assert.That(SortOrder.Ascending.ToString()).IsEqualTo("asc");
        await Assert.That(SortOrder.Descending.ToString()).IsEqualTo("desc");
    }

    [Test]
    public async Task Ascending_And_Descending_AreDifferentInstances()
    {
        await Assert.That(SortOrder.Descending.GetId()).IsNotEqualTo(SortOrder.Ascending.GetId());
        await Assert.That(SortOrder.Descending.ToString()).IsNotEqualTo(SortOrder.Ascending.ToString());
    }

    [Test]
    public async Task Serialization_And_Deserialization_WorksCorrectly()
    {
        var options = new JsonSerializerOptions
        {
            IncludeFields = true
        };

        var ascending = SortOrder.Ascending;
        var json = JsonSerializer.Serialize(ascending, options);
        var deserialized = JsonSerializer.Deserialize<SortOrder>(json, options);

        await Assert.That(deserialized!.GetId()).IsEqualTo(ascending.GetId());
        await Assert.That(deserialized.ToString()).IsEqualTo(ascending.ToString());
    }
}