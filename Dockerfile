﻿FROM harbor.gretzki.ddns.net/containerruntimeglobal/containerruntimeglobal_x64_dotnet_runtime:latest
RUN adduser --system runner
WORKDIR /app
COPY build_output/ ./
EXPOSE 5777
ENV ASPNETCORE_HTTP_PORTS=5777
USER runner
ENTRYPOINT ["dotnet", "DistributedPiCollectorWs.dll"]
